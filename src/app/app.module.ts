import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AccountInfoPage } from '../pages/account-info/account-info';
import { CallBackPage } from '../pages/call-back/call-back';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { DepositPage } from '../pages/deposit/deposit';
import { DepositCashUPage } from '../pages/deposit-cash-u/deposit-cash-u';
import { DepositNetellerPage } from '../pages/deposit-neteller/deposit-neteller';
import { DepositSkrillPage } from '../pages/deposit-skrill/deposit-skrill';
import { DepositVisaPage } from '../pages/deposit-visa/deposit-visa';
import { DocumentPage } from '../pages/document/document';
import { DownloadFilePage } from '../pages/download-file/download-file';
import { ForgetpasswordPage } from '../pages/forgetpassword/forgetpassword';
import { FundingPage } from '../pages/funding/funding';
import { InternalTransgerRequestPage } from '../pages/internal-transger-request/internal-transger-request';
import { LoginPage } from '../pages/login/login';
import { PersonalAPage } from '../pages/personal-a/personal-a';
import { PersonalPPage } from '../pages/personal-p/personal-p';
import { PersonalInfoPage } from '../pages/personal-info/personal-info';
import { RequestNewAccountPage } from '../pages/request-new-account/request-new-account';
import { SendFeedbackPage } from '../pages/send-feedback/send-feedback';
import { VisawithdrawalPage } from '../pages/visawithdrawal/visawithdrawal';
import { WdcashuPage } from '../pages/wdcashu/wdcashu';
import { WithdrawalPage } from '../pages/withdrawal/withdrawal';
import { MyclientPage } from '../pages/myclient/myclient';
import { AffiliateMyClientPage } from '../pages/affiliate-my-client/affiliate-my-client';
import { BankwithdrwalPage } from '../pages/bankwithdrwal/bankwithdrwal';
import { TrackAffiliatePage } from '../pages/track-affiliate/track-affiliate';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { UrlProvider } from '../providers/url/url';
import { CurrencyService } from '../providers/currency-service';
import { FeedbackService } from '../providers/feedback-service';
import { PersonalInfoService } from '../providers/personal-info-service';
import { StaticStrings } from '../providers/static-strings';


import {Network} from '@ionic-native/network';

// import { FileOpener } from '@ionic-native/file-opener';
// import { FilePath } from '@ionic-native/file-path';
import { FileChooser } from '@ionic-native/file-chooser';
// import { Transfer } from '@ionic-native/transfer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import { Clipboard } from '@ionic-native/clipboard';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AccountInfoPage,AffiliateMyClientPage,BankwithdrwalPage,CallBackPage,ChangePasswordPage,DepositPage,
    DepositCashUPage,DepositNetellerPage,DepositSkrillPage,DepositVisaPage,DocumentPage,DownloadFilePage,
    ForgetpasswordPage,FundingPage,InternalTransgerRequestPage,LoginPage,MyclientPage,PersonalAPage,PersonalInfoPage,
    PersonalPPage,RequestNewAccountPage,SendFeedbackPage,TrackAffiliatePage,VisawithdrawalPage,WdcashuPage,WithdrawalPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AccountInfoPage,AffiliateMyClientPage,BankwithdrwalPage,CallBackPage,ChangePasswordPage,DepositPage,
    DepositCashUPage,DepositNetellerPage,DepositSkrillPage,DepositVisaPage,DocumentPage,DownloadFilePage,
    ForgetpasswordPage,FundingPage,InternalTransgerRequestPage,LoginPage,MyclientPage,PersonalAPage,PersonalInfoPage,
    PersonalPPage,RequestNewAccountPage,SendFeedbackPage,TrackAffiliatePage,VisawithdrawalPage,WdcashuPage,WithdrawalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthServiceProvider,
    UrlProvider,CurrencyService,FeedbackService,PersonalInfoService,
    StaticStrings,
    InAppBrowser,
    Network,FileChooser,FileTransfer,File,Clipboard,FileTransferObject,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
