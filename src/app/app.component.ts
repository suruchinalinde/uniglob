import { Component } from '@angular/core';
import { Platform ,AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { HomePage } from '../pages/home/home';
import { ToastController } from 'ionic-angular';
import { LoginPage } from '../pages/login/login';
import{Network}from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  hayWIFI: any;
  rootPage:any = LoginPage;
  public counter=0;

  private online:boolean = false;
private wifi:boolean = false;
public disconnectSubscription:any;
public connectSubscription:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public toastCtrl:ToastController,
  public network:Network,public altercrlt:AlertController) {
    platform.ready().then(() => {


    statusBar.styleDefault();
    splashScreen.hide();

    this.connectSubscription = this.network.onConnect().subscribe(() => {
      
      this.online = true;
      let alrt=this.altercrlt.create({
        title:'Good! Connected To The Internet',
        subTitle:'Internet Successffully Connected ',
        buttons:['OK']
      })
      alrt.present();
      setTimeout(() => {
        // before we determine the connection type.  Might need to wait
        // if (this.network.type === 'wifi') {
        //   this.hayWIFI;
        //   let toast = this.toastCtrl.create({
        //     message:'Wifi Connection is Connected  ',
        //     duration: 500,
        //     position: 'middle',
      
        //   });
        
        //   toast.onDidDismiss(() => {
        //     console.log('Dismissed toast');
        //   });
        
        //   toast.present();
        // }
      }, 3000);
  });
  this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {            
    this.online = false;
    let alrt=this.altercrlt.create({
      title:'Sorry! No Internet Connection',
      subTitle:'Please Connect To Internet and Try Again',
      buttons:['OK']
    })
    alrt.present();

});


platform.registerBackButtonAction(() => {
        if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0 }, 1000)
        } else {
          // console.log("exitapp");
          platform.exitApp();
        }
      }, 0)
    });
  }
    presentToast() {
      let toast = this.toastCtrl.create({
        message: "Press again to exit",
        duration: 3000,
        position: "bottom"
      });
      toast.present();
    }
  
}

