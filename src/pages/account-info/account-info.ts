import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
// import {HeaderComponent} from '../../components/header/header';
import { AlertController, LoadingController, Loading } from 'ionic-angular';

/**
 * Generated class for the AccountInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-info',
  templateUrl: 'account-info.html',
})
export class AccountInfoPage {
datas:any;
loading:Loading;
data={
  account1:'',
  account2:'',
  account3:'',
  account4:'',
  accountType1:'',
  accountType2:'',
  accountType3:'',
  accountType4:'',
  platform1:'',
  platform2:'',
  platform3:'',
  platform4:'',

}

  constructor(public navCtrl: NavController, public navParams: NavParams,public serv : AuthServiceProvider,
   public loadingCtrl:LoadingController) {

    this.fun();

  }

  homefun(){
    this.navCtrl.setRoot(HomePage);
  }

fun(){
// this.compon.showLoading();
this.loading = this.loadingCtrl.create({
  content: 'Please wait...',
  // duration:2000
});
this.loading.present();
  this.serv.getaccountInfo().subscribe(data=>{
    console.log(data);       
        this.loading.dismiss();


this.data.account1=data.accounts.account1;
this.data.accountType1=data.accounts.accountType1;
this.data.platform1=data.accounts.platform1;


this.data.account2=data.accounts.account2;
this.data.accountType2=data.accounts.accountType2;
this.data.platform2=data.accounts.platform2;

this.data.account3=data.accounts.account3;
this.data.accountType3=data.accounts.accountType3;
this.data.platform3=data.accounts.platform3;

this.data.account4=data.accounts.account4;
this.data.accountType4=data.accounts.accountType4;
this.data.platform4=data.accounts.platform4;


  })
}
}
