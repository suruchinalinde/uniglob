import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Loading,AlertController,ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
// import { FileOpener } from '@ionic-native/file-opener';
// import { FilePath } from '@ionic-native/file-path';
import { FileChooser } from '@ionic-native/file-chooser';
// import { Transfer } from '@ionic-native/transfer';
import { FileTransfer ,FileUploadOptions, FileTransferObject,FileUploadResult} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-document',
  templateUrl: 'document.html',
  providers: [FileTransfer, FileTransferObject],

})
export class DocumentPage {

  imgName: any;
  ResponseMsg: any;
  Response: any;
  UserID: any;
  token: any;
  Filaddress: string;
  url: string;
  FileUrl: any;
  loading:Loading;
  public resposeData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private fileChooser: FileChooser,  
    private transfer:FileTransfer, private file: File, public serv:AuthServiceProvider,
  public loadingCtrl:LoadingController , public alertCtrl:AlertController, public toastCtrl:ToastController) {
      this.token=this.serv.currentUser.Token;
      this.UserID=this.serv.currentUser.UserID;
  }

  homefun(){
    this.navCtrl.setRoot(HomePage);
  }


  chooseFile(){
   

    this.url='https://www.uniglobemarkets.com/unibeta/user/api/kvcdocument.php'
    this.fileChooser.open().then(idproof=>{
      this.FileUrl=idproof;
alert(this.FileUrl);
const fileTransfer: FileTransferObject = this.transfer.create();    

let options: FileUploadOptions = {
  fileKey: 'idproof',
  fileName: idproof,
  // chunkedMode: false,
  mimeType:"multipart/form-data",
  // mimeType:"image/jpeg",
  httpMethod : 'POST',
  // mimeType: "file/pdf",
  headers: {},
  params:{
    'token':this.token,
    'idproof':idproof
  }
  
}
// console.log(idproof,this.url, options);
let loading = this.loadingCtrl.create({
  content: "Please wait...",

});
loading.present();

fileTransfer.upload(idproof,this.url, options)
.then((data)=>{
  loading.dismiss();
  // console.log("vjvcjvsj jisdgdcjsvc  "+data);
  // console.log(JSON.stringify(data.response));

  console.log(data.response.split(',')[0]);
  console.log(data.response.split(',')[1]);

  // this.ResponseMsg=data.response.split(',')[0];
  // this.imgName=data.response.split(',')[1];

  let toast = this.toastCtrl.create({
    message:data.response,
    duration: 3000,
    position: 'top'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();


}, (err) => {
  loading.dismiss();
  alert("success"+JSON.stringify(err));
})
    }).catch(e => console.log(e));

  }


  // finalFileUploadfun(){
    
  // }

  finalFileUploadfun(){
  
    // this.url='http://192.168.1.202/projects/uniglobalmarket/user/api/kvcdocument.php'

    this.url='https://www.uniglobemarkets.com/unibeta/user/api/kvcdocument.php'
    this.fileChooser.open().then(addproof=>{
      console.log(addproof);
   this.Filaddress=addproof;
    console.log(addproof);
    
  const fileTransfer: FileTransferObject = this.transfer.create();   

  let options: FileUploadOptions = {
      fileKey: 'addproof',
      fileName: addproof,
      chunkedMode: false,
      mimeType:"multipart/form-data",
      // mimeType: "file/pdf",
      headers: {},
      params:{
        'token':this.token,
       
      }
    }
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
  
    });
    loading.present();
    fileTransfer.upload(addproof ,this.url, options)
    .then((data) => {

      // this.ResponseMsg=data.response.split(',')[0];
      // this.imgName=data.response.split(',')[1];
      loading.dismiss();
      // console.log(data.Object);
      
      let toast = this.toastCtrl.create({
        message: data.response,
        duration: 3000,
        position: 'top'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      toast.present();
  
   
    }, (err) => {
      alert("error"+JSON.stringify(err));
      
    })
  })
    }


    isReadonly() {
      return this.isReadonly;   //return true/false 
    }
    
}


