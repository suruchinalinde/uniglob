import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';
import { AlertController, LoadingController, Loading ,ToastController} from 'ionic-angular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { PersonalInfoService } from '../../providers/personal-info-service';
import { LoginPage } from '../login/login';
import { Clipboard } from '@ionic-native/clipboard';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-personal-p',
  templateUrl: 'personal-p.html',
})
export class PersonalPPage {
  affiliatecode: string;
  link: string;
  mm: string;
  save_response: any;
loading:Loading;
  get_response: any;
  user_type:string;

  personalInfo = {
    firstName:'',
    middleName:'',
    lastName:'',
    phoneNumber:'',
 
  };

  addressDetails = {
    address:'',
    country:'',
    account1:'',
    UserEmail:'',
    business_info:'',
    ib_exp:'',
    estimateno:'',
   
  };
  formgroup: FormGroup;

  firstName:AbstractControl;
  middleName:AbstractControl;
  lastName:AbstractControl;
  phoneNumber:AbstractControl;
  address:AbstractControl;
  country:AbstractControl;
  account1:AbstractControl;
  UserEmail:AbstractControl;
  business_info:AbstractControl;
  ib_exp:AbstractControl;
  estimateno:AbstractControl;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl:LoadingController,  public formbuilder:FormBuilder,public serv:AuthServiceProvider,
  public personalInfoService:PersonalInfoService, public alertCtrl:AlertController,public network:Network,
  private clipboard: Clipboard, public toastCtrl:ToastController) {
    this.user_type=this.serv.currentUser.role;
this.GetPersoalInfo();

// this.addressDetails.ib_exp="yes";
// this.addressDetails.business_info="abc";
// this.addressDetails.estimateno="sdsd";

console.log(this.addressDetails.ib_exp);



this.formgroup = formbuilder.group({

  firstName:['',Validators.required],
  middleName:['',Validators.required],
  lastName:['',Validators.required],
  phoneNumber:['',Validators.required],
  address:['',Validators.required],
  country:['',Validators.required],
  account1:['',Validators.required],
  UserEmail:['',Validators.required],
  business_info:['',Validators.required],
  ib_exp:['',Validators.required],
  estimateno:['',Validators.required],



});


this.firstName=this.formgroup.contains['firstName'];
this.middleName=this.formgroup.contains['middleName'];
this.lastName=this.formgroup.contains['lastName'];
this.phoneNumber=this.formgroup.contains['phoneNumber'];
this.address=this.formgroup.contains['address'];
this.country=this.formgroup.contains['country'];
this.account1=this.formgroup.contains['account1'];
this.UserEmail=this.formgroup.contains['UserEmail'];
this.business_info=this.formgroup.contains['business_info'];
this.ib_exp=this.formgroup.contains['ib_exp'];
this.estimateno=this.formgroup.contains['estimateno'];


  }

  homefun6(){
    this.navCtrl.setRoot(HomePage);
  }

  
  GetPersoalInfo(){


    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['OK']
      });
      alert.present();
      this.navCtrl.setRoot(LoginPage);
      return;
    }

   
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration:2000
    });
    this.loading.present();

    // console.log(this.user_type);
this.personalInfoService.get_personal_info(this.user_type).subscribe(response=>{

  console.log(response);

  this.personalInfoService.setUser(response);

  console.log(response.user.affiliatecode);
this.affiliatecode=response.user.affiliatecode;
console.log(this.affiliatecode);
this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;

  this.get_response = response;
},
error => {
  this.loading.dismiss();

  // this.splash.showError(error);
  let alert = this.alertCtrl.create({
    // title: 'Error',
    subTitle: 'error',
    buttons: ['OK']
  });
  alert.present();
},

() => {

  if (this.get_response.status == 'T_EMPTY') {
    this.loading.dismiss();

    // this.splash.showError("You are logged out. Please Login again.");
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: this.save_response.msg,
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(LoginPage);
    return;
  }
  else if (this.get_response.status == 'T_INVAL') {
    this.loading.dismiss();

    // this.splash.showError("You are logged out. Please Login again.");
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: this.save_response.msg,
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(LoginPage);
    return;
  }

  // populate the user array. 
  this.personalInfoService.populateUserInfo(this.get_response);
  this.personalInfo = this.personalInfoService.userInfo.personalInfo;
  this.addressDetails = this.personalInfoService.userInfo.addressDetails;
  this.loading.dismiss();
}
)
}
 

isReadonly() {
  return this.isReadonly;   //return true/false 
}


PostPersonaInfo()
{ 
  if (this.serv.is_user_authenticated() == false) {
  let alert = this.alertCtrl.create({
    // title: 'Error',
    subTitle: 'You are logged out. Please Login again.',
    buttons: [{
          text:'ok',
          handler:()=>{
            this.navCtrl.setRoot(LoginPage);
          }
        }]
  });
  alert.present();
  // this.splash.showError("You are logged out. Please Login again.");
  // this.navCtrl.setRoot(LoginPage);
  return;
}

if(this.network.type === 'none')
{
let alert = this.alertCtrl.create({
title: 'Sorry ! No Internet Connection ',
subTitle:'Please Connect To Internet And Try Again ',
buttons: ['ok']
});
alert.present();
}
else{


  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    // duration:1500

  });
  this.loading.present();
  // this.splash.showLoading();
  
  console.log(this.personalInfo, this.addressDetails ,this.user_type);
  this.personalInfoService.save_personal_info(this.personalInfo, this.addressDetails, this.user_type)
    .subscribe(response => {
      console.log(response);
      // console.log(response.user.affiliatecode);
 
// this.affiliatecode=response.user.affiliatecode;
// console.log(this.affiliatecode);

      this.save_response = response;
      // console.log(response);
      // this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;
 
   
    // () => {
      // this.loading.dismiss();

      if (this.save_response.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.save_response.msg,
          buttons: [{
            text:'ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.save_response.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.save_response.msg,
          buttons: [{
            text:'ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.save_response.status == "success") {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.save_response.message,
          buttons: [{
            text:'ok',
            // handler:()=>{
            //   this.navCtrl.setRoot(PersonalInfoPage);
            // }
          }]
        });
        alert.present();
     
      }
      // else {
      //   let alert = this.alertCtrl.create({
      //     title: 'Error',
      //     subTitle: 'Error! Information could not be saved',
      //     buttons: ['OK']
      //   });
      //   alert.present();
      //   // this.splash.showError("Error! Information could not be saved.");
      // }

    // }
    }, error =>{
      this.loading.dismiss();

      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'error',
        buttons: ['OK']
      });
      alert.present();
    //  this.splash.showError(error),
    },
  // console.log(this.personalInfo,this.addressDetails);
  // console.log(this.personalInfo,this.addressDetails);
  // this.personalInfoService.save_personal_info(this.personalInfo,this.addressDetails).subscribe(data=>{
  //   if(data.status=='success'){

  //   }
  // })
  )
}
}

ngIfCtrl(){
  // let alrt =this.alertCtrl.create({
  //   title:'Link Copied  Successfully ',
  //   subTitle:this.mm,
  //   buttons: ['OK']
      
  //   })
  //   alrt.present();
  let toast = this.toastCtrl.create({
    message: 'Link Copied  Successfully ',
    duration: 1000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
  //  this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;
   this.link=this.mm;
   console.log(this.link);
   this.clipboard.copy(this.link);

   this.clipboard.paste().then(
    (resolve: string) => {
      let alrt =this.alertCtrl.create({
        title:'Copy Link',
        buttons: ['OK']
          
        })
     },
     (reject: string) => {
      //  alert('Error: ' + reject);
     }
   );

 }
}
