import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PersonalPPage } from './personal-p';

@NgModule({
  declarations: [
    PersonalPPage,
  ],
  imports: [
    IonicPageModule.forChild(PersonalPPage),
  ],
})
export class PersonalPPageModule {}
