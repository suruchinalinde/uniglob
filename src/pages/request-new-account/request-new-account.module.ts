import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestNewAccountPage } from './request-new-account';

@NgModule({
  declarations: [
    RequestNewAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestNewAccountPage),
  ],
})
export class RequestNewAccountPageModule {}
