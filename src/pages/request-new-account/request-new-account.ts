import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {  AlertController,LoadingController,Loading} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network'

@IonicPage()
@Component({
  selector: 'page-request-new-account',
  templateUrl: 'request-new-account.html',
})
export class RequestNewAccountPage {
  datas:any;
  id:any;
platforms:any;
tradings:any;
loading:Loading;
save_response:any;

formgroup: FormGroup;
accountrype:AbstractControl;
requestoffer:AbstractControl;
tradingplatform:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public serv:AuthServiceProvider,public loadingCtrl:LoadingController, public alertCtrl:AlertController,
  public formbuilder:FormBuilder, public network:Network) {

    this.formgroup = formbuilder.group({
      accountrype:[Validators.required],
      requestoffer:[Validators.required],
      tradingplatform:[Validators.required]
    });

    this.accountrype= this.formgroup.contains['accountrype'];
    this.requestoffer= this.formgroup.contains['requestoffer'];
    this.tradingplatform= this.formgroup.contains['tradingplatform'];



    this.getnewdata();
  }
  getnewdata(){
    //if token not present
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['OK']
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }

    
    // locading add
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1000
    });
    this.loading.present();


// get the user info service call
    this.serv.getnewdata().subscribe(data=>{
      this.loading.dismiss();

      console.log(data);
      console.log(data.requestoffer);
      this.datas=data.accountrype;
   this.platforms=data.requestoffer;
   this.tradings=data.tradingplatform;
     
    })
    
  }


  newRequest(accountrype, requestoffer, tradingplatform){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1000
    });
    this.loading.present();

this.serv.sendnewACrequestt(accountrype, requestoffer, tradingplatform).subscribe(data=>{
  this.loading.dismiss();
  this.save_response = data;
  console.log(data);
  // this.loading.dismiss();


  if (this.save_response.status == 'T_EMPTY') {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: this.save_response.message,
      buttons: [{
        text:'ok',
        handler:()=>{
          this.navCtrl.setRoot(LoginPage);
        }
      }]
    });
    alert.present();
    // this.splash.showError("You are logged out. Please Login again.");
    // this.navCtrl.setRoot(LoginPage);
    return;
  }
  else if (this.save_response.status == 'T_INVAL') {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: this.save_response.message,
      buttons: [{
        text:'ok',
        handler:()=>{
          this.navCtrl.setRoot(LoginPage);
        }
      }]
    });
    alert.present();
    // this.splash.showError("You are logged out. Please Login again.");
    return;
  }

  if (this.save_response.status == "success") {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: this.save_response.message,
      buttons: [{
        text:'ok',
        handler:()=>{
          this.navCtrl.setRoot(HomePage);
        }
      }]
    });
    alert.present();
    // this.splash.showSuccess("Information saved successfully.");
    // this.navCtrl.setRoot(HomePage);
  }

})
  }
  }

  homefun7(){
    this.navCtrl.setRoot(HomePage);
  }
}
