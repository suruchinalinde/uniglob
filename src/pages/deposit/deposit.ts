import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Platform,ToastController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { FundingPage } from '../funding/funding';
import { DepositNetellerPage } from '../deposit-neteller/deposit-neteller';
import { DepositSkrillPage } from '../deposit-skrill/deposit-skrill';
import { DepositVisaPage } from '../deposit-visa/deposit-visa';
import { DepositCashUPage } from '../deposit-cash-u/deposit-cash-u';
import {PersonalInfoService} from '../../providers/personal-info-service';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {UrlProvider  } from '../../providers/url/url';


@IonicPage()
@Component({
  selector: 'page-deposit',
  templateUrl: 'deposit.html',
})
export class DepositPage {
  urllink: any;
  spinnerDialog: any;
  token: any;
  mm: string;
user_type:any;
  datas:any;
  icon:any;
  main:any;
  transferinfos:any;
  
  topics:any;
  List:any;
  myInput:any;
  public counter=1;

  user_name:any=[];
  merchantRefId:any=[];
  paymentdate:any=[];
  status:any=[];
  paytype:any=[];
  currency:any=[];
  amount:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public personalserv:PersonalInfoService,
  public serv:AuthServiceProvider, public platform:Platform , public inAppBrowser:InAppBrowser,
  public toastCtrl:ToastController,public url:UrlProvider) {
this.token=this.serv.currentUser.Token;
this.urllink=url.Url;
console.log(this.urllink);
    this.mm="https://www.uniglobemarkets.com/unibeta/user/api/paymentgateway/wirecapital/index.php ";
    var target = "_blank";
    var options= {
      params:{
        'token':this.token    
      }
    }
    this.serv.GetTransactionInfoFun().subscribe(data=>{
      // console.log(data.history[0] );
      this.List=data.history;
  console.log(this.List);
      this.topics=this.List;
      console.log(this.topics);
    });
    // this.fun();
// this.List=this.navParams.get('transferinfo');
// this.topics=this.List;
this.user_type=this.serv.currentUser.role;

this.user_name=[];
this.merchantRefId=[];
this.paymentdate=[];
this.status=[];
this.paytype=[];
this.currency=[];
this.amount=[];


    this.List="";

    this.topics="";

    this.datas=this.navParams.get('icon');

  

  // this.fumm();
}
fun(){
  this.serv.GetTransactionInfoFun().subscribe(data=>{
    // console.log(data.history[0] );
  
    this.List=data.history;
console.log(this.List);
    this.topics=this.List;
// this.topics.ammountcode=data.amount + ' ' + data.currency;
// console.log(this.topics.ammountcode);
    for (let i = 0; i=20; i++) {
      this.topics.push( this.topics.length);
    }
  });
}

// fumm(){
//   for (let i = 0; i < 3; i++) {
//     this.topics.push( this.topics.length);
//   }
// }
doInfinite(infiniteScroll) {
  console.log('Begin async operation');
  // for (let i = 0; i < 30; i++) {
  //   this.topics.push( this.topics.length);
  // } 
  setTimeout(() => {
    for (let i = 0; i < 0; i++) {
      this.topics.push(this.topics.length );
    }

    console.log('Async operation has ended');
    infiniteScroll.complete();
  }, 500);
}
 

  imgMainfun(name){
    console.log(name);
    // name=name.toLowerCase();
    // console.log(name);
    switch (name)
    {
      case "CashU":     this.navCtrl.setRoot(DepositCashUPage,{datas:this.datas});
                        break;

     case "Neteller":   this.navCtrl.setRoot(DepositNetellerPage,{datas:this.datas});
                        break;

    case "Skrill":   this.navCtrl.setRoot(DepositSkrillPage,{datas:this.datas});
                        break;

    // case "paypal":   this.navCtrl.setRoot(DepositNetellerPage);
    //                     break;

    // case "qiwi":   this.navCtrl.setRoot(DepositNetellerPage);
    //                     break; 

  //   case "webmoney":   this.navCtrl.setRoot(DepositNetellerPage);
  //                       break;

  //  case "okpay":   this.navCtrl.setRoot(DepositNetellerPage);
  //                       break;
//  case "Wirecapital":   this.navCtrl.setRoot(DepositVisaPage,{datas:this.datas});
//                         break;

case "Wirecapital":  
var target = "_self";
var options= {
  location:'no'
}
const browser = this.inAppBrowser.create("https://www.uniglobemarkets.com/unibeta/user/api/paymentgateway/wirecapital/index.php?token="+this.token, target, {
  location: 'no',

hardwareback:'no'
});
browser.show();
browser.on('loadstart').subscribe((eve) => {
console.log(eve);


})
    }
  }

  getinfofun(){

    this.personalserv.get_personal_info(this.user_type) .subscribe(response => {
this.personalserv.setUser(response);
    console.log(response);
      })
  }


  dpnetellerfun(){
    this.navCtrl.setRoot(DepositNetellerPage);
  }
  dpskrillfun(){
    this.navCtrl.setRoot(DepositSkrillPage);
  }
  dpVisafun(){
    this.navCtrl.setRoot(DepositVisaPage);

  }

  dpcashufun(){
    this.navCtrl.setRoot(DepositCashUPage);
  }

  onSearchInput($event){
    this.topics=Array();
    console.log(this.topics)
    for (var i = 0; i < this.List.length; i++) {
      
            if (this.List[i].user_name.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].merchantRefId.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].paymentdate.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].status.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].paytype.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].amount.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].currency.toLowerCase().search(this.myInput.toLowerCase()) != -1 ) {
              this.topics.push(this.List[i]);
            }
          }
      
          if (this.topics.length == 0) {
          }
  }
  homefun11(){
 this.navCtrl.setRoot(FundingPage);
  }
}
