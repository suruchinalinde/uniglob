import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DepositPage } from '../deposit/deposit';
import {PersonalInfoService} from '../../providers/personal-info-service';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service'
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';

/**
 * Generated class for the DepositSkrillPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deposit-skrill',
  templateUrl: 'deposit-skrill.html',
})
export class DepositSkrillPage {

  firstName: any;
  lastName: any;
  phoneNumber:any;
  zipCode:any;
  city:any;
  country: any;
  email:any;
  main:any;


  formgroup:FormGroup;

  firstNamee:AbstractControl;
  lastNamee: AbstractControl;
  phoneNumbere:AbstractControl;
  zipCodee:AbstractControl;
  citye:AbstractControl;
  countrye: AbstractControl;
  emaile:AbstractControl;
  scurrencye:AbstractControl;
  amounte:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams,public personalInfoService:PersonalInfoService,
  public serc:AuthServiceProvider, public formbuilder:FormBuilder) {
    console.log(this.personalInfoService.getinfo);
    this.firstName=this.personalInfoService.getinfo.firstName;
    this.lastName=this.personalInfoService.getinfo.lastName;
    this.phoneNumber=this.personalInfoService.getinfo.phoneNumber;
    this.zipCode=this.personalInfoService.getinfo.zipCode;
    this.country=this.personalInfoService.getinfo.country;
    this.city=this.personalInfoService.getinfo.city;
    this.email=this.serc.currentUser.userEmail;


    this.main=this.navParams.get('datas');

    


this.formgroup = formbuilder.group({

  firstNamee:[Validators.required],
  lastNamee: [Validators.required],
  phoneNumbere:[Validators.required,Validators.minLength(10)],
  zipCodee:[Validators.required],
  citye:[Validators.required],
  countrye: [Validators.required],
  emaile:[Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
  scurrencye:[Validators.required],
  amounte:[Validators.required],


});

// newComment = new FormControl('', [validateEmptyField]);



this.firstNamee= this.formgroup.contains['firstNamee'];
this.lastNamee= this.formgroup.contains['lastNamee']; 
this.phoneNumbere= this.formgroup.contains['phoneNumbere'];
this.zipCodee= this.formgroup.contains['zipCodee'];
this.citye= this.formgroup.contains['citye'];
this.countrye= this.formgroup.contains['countrye']; 
this.emaile= this.formgroup.contains['emaile'];
this.scurrencye= this.formgroup.contains['scurrencye'];
this.amounte= this.formgroup.contains['amounte'];



    

  }

  homefun14(){
    this.navCtrl.setRoot(DepositPage,{icon:this.main});
  }

  depositskrillfun(){
    console.log('mm');
  }

  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  

}
