import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepositSkrillPage } from './deposit-skrill';

@NgModule({
  declarations: [
    DepositSkrillPage,
  ],
  imports: [
    IonicPageModule.forChild(DepositSkrillPage),
  ],
})
export class DepositSkrillPageModule {}
