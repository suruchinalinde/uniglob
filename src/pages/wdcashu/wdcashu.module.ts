import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WdcashuPage } from './wdcashu';

@NgModule({
  declarations: [
    WdcashuPage,
  ],
  imports: [
    IonicPageModule.forChild(WdcashuPage),
  ],
})
export class WdcashuPageModule {}
