import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { WithdrawalPage } from '../withdrawal/withdrawal';
import {FeedbackService} from '../../providers/feedback-service';
import {AlertController,Loading, LoadingController} from 'ionic-angular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service'
import { LoginPage } from '../login/login';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-wdcashu',
  templateUrl: 'wdcashu.html',
})
export class WdcashuPage {
  datas:any;
get_response:any;
loading:Loading;
email:any;
accounts:any;
gets:any;
name:any;
get_rser:any;
ewallet:any;
main:any;
Useremail:any;

formgroup: FormGroup;
ewalletl:AbstractControl;
account:AbstractControl;
emaill:AbstractControl;
amountt:AbstractControl;
comment:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public feedbackserv:FeedbackService,
  public alertCtrl:AlertController, public loadingCtrl:LoadingController,public serv:AuthServiceProvider,
  public formbuilder:FormBuilder, public network :Network)
   {
    this.formgroup = formbuilder.group({
      ewalletl:[Validators.required],
      account:[Validators.required],
     emaill:[      Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      amountt:[Validators.required],
      comment: [null,Validators.required], 

    });
    
    // newComment = new FormControl('', [validateEmptyField]);

    this.ewalletl= this.formgroup.contains['ewalletl'];
    this.account= this.formgroup.contains['account'];
    this.emaill= this.formgroup.contains['emaill'];
    this.amountt= this.formgroup.contains['amountt'];
    this.comment= this.formgroup.contains['comment'];




this.ewallet=this.navParams.get('name');
this.main=this.navParams.get('datas');

console.log(this.main);
     console.log(this.feedbackserv.AccountData.account);
 this.accounts=this.feedbackserv.AccountData.account;
console.log(this.accounts);

console.log(serv.currentUser.userEmail);
this.email=this.serv.currentUser.userEmail;
this.getEwalletfun()

  }
//  public validateEmptyField(c: FormControl) {
//     return c.value && !c.value.trim() ? {
//       required: {
//         valid: false
//       }
//     } : null;
//   }
  


  homefun18(){
    this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
  }

  getEwalletfun(){

    //if token  not present 
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['Ok']
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }

//loading 

this.loading=this.loadingCtrl.create({
  content:'Please wait',
  // duration:1000
})
this.loading.present();

//service call
    this.serv.getEwallet().subscribe(data=>{
      this.loading.dismiss();
      console.log(data);
      this.gets=data.accounts;

    },
    error => {
      this.loading.dismiss();

      // this.splash.showError(error);
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    
  })
    
  }

  EwalletCASHFun(ewallet,account,email,amount,comment){
    
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: [{
              text:'Ok',
              handler:()=>{
                this.navCtrl.setRoot(LoginPage);
              }
            }]
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      // this.navCtrl.setRoot(LoginPage);
      return;
    }
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1000
  
    });
    this.loading.present();
  
    
    this.serv.PostEwalletFUN(ewallet,account,email,amount,comment).subscribe(data=>{
      console.log(data);
     

      this.get_rser = data;
      console.log(this.get_rser);
    
   
    // () => {
      // this.loading.dismiss();

      if (this.get_rser.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:  this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.get_rser.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:  this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.get_rser.status == "success") {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
      if (this.get_rser.error == "success") {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }

    },error =>{
      this.loading.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    //  this.splash.showError(error),
    },
  )
}
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }

  onKeyPress(event) {
    if ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122) || event.keyCode == 32 || event.keyCode == 46) {
        return true;
    }
    else {
        return false;
    }
}
  
}
