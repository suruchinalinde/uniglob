import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,LoadingController, Loading} from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {FeedbackService} from '../../providers/feedback-service';
import { LoginPage } from '../login/login';
import { FormControl, FormGroup, Validators,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';


@IonicPage()
@Component({
  selector: 'page-send-feedback',
  templateUrl: 'send-feedback.html',
})
export class SendFeedbackPage {
  fatch: boolean;
 Token:any;
// id:any;
email:any;
name:any;

formgroup: FormGroup;
accountt:AbstractControl;
type: AbstractControl;
namee: AbstractControl;
emaill:AbstractControl;
message:AbstractControl;

feedback = {
  account: '',
  type: '',
  name: '',
  email: '',
  message: ''
};

 datas:any;
 loading:Loading;
 get_response:any;
 send_response:any;
 re:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public serv:AuthServiceProvider,
  public alertCtrl:AlertController, public feedbackserv:FeedbackService , public loadingCtrl:LoadingController,
  public formbuilder:FormBuilder,public network:Network) {

    this.formgroup = formbuilder.group({
      accountt:[Validators.required],
      type:[Validators.required],
     emaill:[Validators.required],
     namee:[Validators.required],
     message: [null,Validators.required], 

    });
    
    // newComment = new FormControl('', [validateEmptyField]);

    this.type= this.formgroup.contains['type'];
    this.accountt= this.formgroup.contains['accountt'];
    this.emaill= this.formgroup.contains['emaill'];
    this.namee= this.formgroup.contains['namee'];
    this.message= this.formgroup.contains['message'];



this.feedback.name = this.serv.currentUser.firstName + ' ' + this.serv.currentUser.middleName + ' ' + this.serv.currentUser.lastName;
this.feedback.email = this.serv.currentUser.userEmail;

this.Token=this.serv.currentUser.Token;
this.get_accounts();
this.re = '^[a-zA-Z]+$';

  }

  allowedfun(message){
    console.log(message);
    if(message !=this.re ){
      this.fatch=false;
    }
    else{
      this.fatch=true;
    }
  }

  homefun8(){
    this.navCtrl.setRoot(HomePage);
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }


  public get_accounts() {
    // check for user authentication
    if (this.serv.is_user_authenticated() == false) {

    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: 'You are logged out. Please Login again',
      buttons: ['OK']
    });
    alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 1000
    });
    this.loading.present();

    // this.splash.showLoading();

    if (this.feedbackserv.accountsSaved == 1) {
      this.datas = this.feedbackserv.accounts;
      this.loading.dismiss();

    }
    else {
      this.feedbackserv.get_accounts() .subscribe(response => {
          console.log(response);
          this.get_response = response;
          console.log(this.get_response);
        },
        error => {
          
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: 'error',
      buttons: ['OK']
    });
    alert.present();
          // this.splash.showError(error),
        },  
        () => {

          if (this.get_response.status == 'success') {
            this.loading.dismiss();
            this.feedbackserv.populate_accounts(this.get_response);
            this.datas = this.feedbackserv.accounts;
            
          }
          else {
            this.loading.dismiss();
            let alert = this.alertCtrl.create({
              // title: 'Error',
              subTitle: this.get_response.msg,
              buttons: ['OK']
            });
            alert.present();
            // this.splash.showError(this.get_response.msg);
            this.navCtrl.setRoot(HomePage);
          }
        }
        );
    }
  }

  send_feedback() {
    // this.splash.showLoading();
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration: 3000
    });
    this.loading.present();

console.log(this.feedback);

    this.feedbackserv.send_feedback(this.feedback)
      .subscribe(response => {
        console.log(response);
        this.send_response = response;
        // console.log(response);

        if (this.send_response.status == "success") {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            // title: 'Success',
            subTitle: this.send_response.msg,
            buttons: [{
              text:'ok',
              handler:()=>{
                this.navCtrl.setRoot(HomePage);
              }
            }]
          });
          alert.present();
          // this.splash.showSuccess(this.send_response.msg);
          // this.navCtrl.setRoot(HomePage);
        }
        else if (this.send_response.status == "fail") {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            // title: 'Error',
            subTitle: this.send_response.msg,
            buttons: ['OK']
          });
          alert.present();
          // this.splash.showError("There was some error! Please try again.");
        }
      },
      error =>{
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: 'error',
          buttons: ['OK']
        });
        alert.present();
      //  this.splash.showError(error),
      },
      () => {

        if (this.send_response.status == 'T_EMPTY') {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            // title: 'Error',
            subTitle: this.send_response.msg,
            buttons: ['OK']
          });
          alert.present();
          // this.splash.showError("You are logged out. Please Login again.");
          this.navCtrl.setRoot(LoginPage);
          return;
        }
        else if (this.send_response.status == 'T_INVAL') {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            // title: 'Error',
            subTitle: this.send_response.msg ,
            buttons: ['OK']
          });
          alert.present();
          // this.splash.showError("You are logged out. Please Login again.");
          this.navCtrl.setRoot(LoginPage);
          return;
        }
    
      
      });
    }
  }

  onKeyPress(event) {
    if ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122) || event.keyCode == 32 || event.keyCode == 46) {
        return true;
    }
    else {
        return false;
    }
}
  }

