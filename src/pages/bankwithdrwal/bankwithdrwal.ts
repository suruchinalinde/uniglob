import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WithdrawalPage } from '../withdrawal/withdrawal';
import {FeedbackService} from '../../providers/feedback-service';
import {Loading,LoadingController,AlertController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { WdcashuPage } from '../wdcashu/wdcashu';

/**
 * Generated class for the BankwithdrwalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bankwithdrwal',
  templateUrl: 'bankwithdrwal.html',
})
export class BankwithdrwalPage {
  datas:any;
loading:Loading;
main:any;
get_rser:any;
  constructor(public navCtrl: NavController, public navParams: NavParams , public feebcakserv:FeedbackService,
  public lodaingcrlt:LoadingController, public serv:AuthServiceProvider, public alertCtrl:AlertController) {
    this.main=this.navParams.get('datas');

    // this.fun();
    console.log(this.feebcakserv.AccountData);
    this.datas=this.feebcakserv.AccountData.account;
  }
// fun(){
  
//   this.loading = this.lodaingcrlt.create({
//     content: 'Please wait...',
//     duration:2000
//   });
//   this.loading.present();

// }
  homefun17(){
    this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
  }

  BankWithdrawalfun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo){
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['Ok']
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }
    this.loading=this.lodaingcrlt.create({
      content:'Please wait',

    })
    this.loading.present();


    this.serv.BankWithdrawalfun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo).subscribe(data=>{
      console.log(data);
      this.loading.dismiss();
      this.get_rser=data;
     
      if (this.get_rser.status == 'T_EMPTY') {
        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.get_rser.status == 'T_INVAL') {
        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.get_rser.status == "success") {
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
    },error => {
      // this.splash.showError(error);
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    
  })
  }
}
