import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankwithdrwalPage } from './bankwithdrwal';

@NgModule({
  declarations: [
    BankwithdrwalPage,
  ],
  imports: [
    IonicPageModule.forChild(BankwithdrwalPage),
  ],
})
export class BankwithdrwalPageModule {}
