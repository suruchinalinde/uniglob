import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,LoadingController,Loading} from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';



@IonicPage()
@Component({
  selector: 'page-affiliate-my-client',
  templateUrl: 'affiliate-my-client.html',
})
export class AffiliateMyClientPage {
  topics:string[];
  List:any[];
  myInput:any;
  loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams,public serv :AuthServiceProvider,
  public alertCtrl:AlertController,public loadingCtrl:LoadingController) {
    this.getMyClientdata();
    this.List=[];
    this.topics=[];
   
  }

  homefunpage(){
    this.navCtrl.setRoot(HomePage);
  }
  getMyClientdata(){

    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['OK']
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }
  
    // this.splash.showLoading();
    //  this.navCtrl.setRoot(PersonalInfoPage);
  
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration:2000
    });
    this.loading.present();
  
    this.serv.AffiliateMyClientFun().subscribe(data=>{
      console.log(data.clients);
      this.List=data.clients;
      this.topics=this.List;
      this.loading.dismiss();
    })
  }

  onSearchInput($event){
    this.topics=Array();
    console.log(this.topics)
    for (var i = 0; i < this.List.length; i++) {
      
            if (this.List[i].account_no_1.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].FirstName.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].LastName.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].UserEmail.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].PhoneNumber.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].country.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].city.toLowerCase().search(this.myInput.toLowerCase()) != -1 ) {
              this.topics.push(this.List[i]);
            }
          }
      
          if (this.topics.length == 0) {
          }
  }
}
