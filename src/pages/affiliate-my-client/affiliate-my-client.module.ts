import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AffiliateMyClientPage } from './affiliate-my-client';

@NgModule({
  declarations: [
    AffiliateMyClientPage,
  ],
  imports: [
    IonicPageModule.forChild(AffiliateMyClientPage),
  ],
})
export class AffiliateMyClientPageModule {}
