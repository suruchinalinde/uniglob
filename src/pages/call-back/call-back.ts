import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,Loading,LoadingController} from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {LoginPage} from '../login/login';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
   import {Network} from '@ionic-native/network'

/**
 * Generated class for the CallBackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-call-back',
  templateUrl: 'call-back.html',
})
export class CallBackPage {
  flat: boolean;
  loading:Loading;
  send_response:any;
//   callBackInfo={
//     phoneNumber : '',
//     timeslot : ''
// }

formgroup:FormGroup;
phonenumber:AbstractControl;
timeslot:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public serv:AuthServiceProvider, public alertCtrl:AlertController, public loadingCtrl:LoadingController,
public formbuilder:FormBuilder, public network:Network) {

  this.formgroup = formbuilder.group({
    phonenumber:['',[Validators.minLength(9)]],
    timeslot:[Validators.required],


  });
  
  // newComment = new FormControl('', [validateEmptyField]);

  this.phonenumber= this.formgroup.contains['phonenumber'];
  this.timeslot= this.formgroup.contains['timeslot'];
 


  }

  // NumberChange(phonenumber){
  //   console.log(phonenumber)
  //   this.flat=true;
  // }
  
  // mainfun(phonenumber){
    
  //   if(phonenumber.length > 15){
  //        this.flat=false;
  //   }
  //   else{
  //     this.flat=true;
  //   }
  // }

  homefun1(){
    this.navCtrl.setRoot(HomePage);
  }

  callbackfun(phonenumber,timeslot){

        if (this.serv.is_user_authenticated() == false) {
        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: 'You are logged out. Please Login again.',
          buttons: ['Ok']
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        this.navCtrl.setRoot(LoginPage);
        return;
      }
  
      if(this.network.type === 'none')
      {
    let alert = this.alertCtrl.create({
      title: ' Sorry ! No Internet Connection ',
      subTitle:'Please Connect To Internet And Try Again ',
      buttons: ['ok']
    });
    alert.present();
    }
    else{
      // this.splash.showLoading();
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration:2000
      });
      this.loading.present();


    console.log(phonenumber,timeslot);
    this.serv.callback(phonenumber,timeslot).subscribe(data=>{
     this.loading.dismiss();
      console.log(data);
this.send_response=data;

      if (this.send_response.status == 'success') {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.send_response.msg,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(HomePage);
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess(this.send_response.msg);
        // this.navCtrl.setRoot(HomePage);
      }
      else if (this.send_response.status == 'fail') {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'fail',
          subTitle: this.send_response.msg,
          buttons: ['Ok']
        });
        alert.present();
        // this.splash.showError("There was some error! Please try again.");
      }
  
    },
    error =>{
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    //  this.splash.showError(error),
    },
    () => {
      
              if (this.send_response.status == 'T_EMPTY') {
                let alert = this.alertCtrl.create({
                  // title: 'Error',
                  subTitle: this.send_response.msg,
                  buttons: ['Ok']
                });
                alert.present();
                // this.splash.showError("You are logged out. Please Login again.");
                this.navCtrl.setRoot(LoginPage);
                return;
              }
              else if (this.send_response.status == 'T_INVAL') {
                let alert = this.alertCtrl.create({
                  // title: 'Error',
                  subTitle: this.send_response.msg,
                  buttons: ['Ok']
                });
                alert.present();
                // this.splash.showError("You are logged out. Please Login again.");
                this.navCtrl.setRoot(LoginPage);
                return;
              }
          
            
            })
          }
  }
}
