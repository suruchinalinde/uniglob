import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallBackPage } from './call-back';

@NgModule({
  declarations: [
    CallBackPage,
  ],
  imports: [
    IonicPageModule.forChild(CallBackPage),
  ],
})
export class CallBackPageModule {}
