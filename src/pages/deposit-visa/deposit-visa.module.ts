import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepositVisaPage } from './deposit-visa';

@NgModule({
  declarations: [
    DepositVisaPage,
  ],
  imports: [
    IonicPageModule.forChild(DepositVisaPage),
  ],
})
export class DepositVisaPageModule {}
