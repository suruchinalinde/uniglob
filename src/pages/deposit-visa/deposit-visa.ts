import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DepositPage } from '../deposit/deposit';
import {CurrencyService} from '../../providers/currency-service';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {PersonalInfoService} from '../../providers/personal-info-service'
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';


@IonicPage()
@Component({
  selector: 'page-deposit-visa',
  templateUrl: 'deposit-visa.html',
})
export class DepositVisaPage {
  mm: any ;

  firstName: any;
  lastName: any;
  phoneNumber: any;

  zipCode: any;
  country: any;
  city: any;
  email:any;
  main:any;
years:any;

formgroup:FormGroup;
firstNamee: AbstractControl;
  lastNamee: AbstractControl;
  phoneNumbere: AbstractControl;
  zipCodee: AbstractControl;
  countrye: AbstractControl;
  citye: AbstractControl;
  emaile:AbstractControl;
  scodee:AbstractControl;
  amounte:AbstractControl;
  sCurrencye:AbstractControl;
  cardNUme:AbstractControl;
  cardnamee:AbstractControl;
  monthe:AbstractControl;
  yeare:AbstractControl;
  ccve:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public currsev:CurrencyService,
  public serv:AuthServiceProvider, public personalInfoSerc:PersonalInfoService,public formbuilder:FormBuilder,
public network:Network) {

    //get the personal user info from personalinfo service
    this.firstName=this.personalInfoSerc.getinfo.firstName;
    this.lastName=this.personalInfoSerc.getinfo.lastName;
    this.phoneNumber=this.personalInfoSerc.getinfo.phoneNumber;
    this.zipCode=this.personalInfoSerc.getinfo.zipCode;
    this.country=this.personalInfoSerc.getinfo.country;
    this.city=this.personalInfoSerc.getinfo.city;
    this.email=this.serv.currentUser.userEmail;

    
    this.main=this.navParams.get('datas');
this.years=this.currsev.yearm;



this.formgroup = formbuilder.group({

  firstNamee:[Validators.required],
  lastNamee: [Validators.required],
  phoneNumbere: [Validators.required,Validators.minLength(10)],
  zipCodee: [Validators.required],
  countrye: [Validators.required],
  citye: [Validators.required],
  emaile:[Validators.required,Validators.email],
  scodee:['',Validators.required],
  amounte:[Validators.required],
  sCurrencye:[Validators.required],
  cardNUme:[Validators.required],
  cardnamee:['',Validators.required],
  monthe:[Validators.required],
  yeare:[Validators.required],
  ccve:[Validators.required],
});

// newComment = new FormControl('', [validateEmptyField]);



this.firstNamee= this.formgroup.contains['firstNamee'];
this.lastNamee= this.formgroup.contains['lastNamee'];
this.phoneNumbere=this.formgroup.contains['phoneNumbere']; 
this.zipCodee= this.formgroup.contains['zipCodee'];
this.countrye= this.formgroup.contains['countrye'];
this.citye= this.formgroup.contains['citye'];
this.emaile=this.formgroup.contains['emaile'];
this.scodee=this.formgroup.contains['scodee'];
this.amounte=this.formgroup.contains['amounte'];
this.sCurrencye=this.formgroup.contains['sCurrencye'];
this.cardNUme=this.formgroup.contains['cardNUme'];
this.cardnamee=this.formgroup.contains['cardnamee'];
this.monthe=this.formgroup.contains['monthe'];
this.yeare=this.formgroup.contains['yeare'];
this.ccve=this.formgroup.contains['ccve'];



  }

  homefun15(){
    this.navCtrl.setRoot(DepositPage,{icon:this.main});
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  funn(){
    console.log(this.mm);
    
    // console.log(year);
  }
  fun1(year){
    console.log(year);
  }
}
