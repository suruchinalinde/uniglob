import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,LoadingController,Loading,ToastController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Clipboard } from '@ionic-native/clipboard';
import { LoginPage } from '../login/login';
/**
 * Generated class for the TrackAffiliatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-track-affiliate',
  templateUrl: 'track-affiliate.html',
})
export class TrackAffiliatePage {
  visitors: any;
  clients: any;
  link: string;
  mm: string;

hide:any=false;
affiliatecode:any;
gets:any;
loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams ,public serv:AuthServiceProvider,
  public clipboard:Clipboard,public alertCtrl:AlertController,public loadingCtrl:LoadingController,
public toastCtrl:ToastController) {
this.affiliatecode=this.serv.currentUser.affiliatecode;
this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;

    this.trackaffiliateFunction();
  }

  trackaffiliateFunction(){
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: [{
              text:'ok',
              handler:()=>{
                this.navCtrl.setRoot(LoginPage);
              }
            }]
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      // this.navCtrl.setRoot(LoginPage);
      return;
    }

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1500
  
    });
    this.loading.present();
    this.serv.Trackaffiliatefun().subscribe(data=>{
 
      
      console.log(data.visitors,data.clients);
this.clients=data.clients.clients;
this.visitors=data.visitors.visitors;
this.loading.dismiss();
// this.visitors=data;
// console.log(this.clients,this.visitors);
    })
  }
  homefuntracker(){
    this.navCtrl.setRoot(HomePage);
  }
  datafun(hide){
    hide=false;
  }

  ngIfCtrl(){
    // let alrt =this.alertCtrl.create({
    //   title:'Link Copied  Successfully ',
    //   subTitle:this.mm,
    //   buttons: ['OK']
        
    //   })
    //   alrt.present();
    let toast = this.toastCtrl.create({
      message: 'Link Copied  Successfully ',
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
    // this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;
    this.link=this.mm;
    console.log(this.link);
    this.clipboard.copy(this.link);
 
    this.clipboard.paste().then(
     (resolve: string) => {
       let alrt =this.alertCtrl.create({
         title:'Copy Link',
         buttons: ['OK']
           
         })
      },
      (reject: string) => {
       //  alert('Error: ' + reject);
      }
    );
 
  }

  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
}
