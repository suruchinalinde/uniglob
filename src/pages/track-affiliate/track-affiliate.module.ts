import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackAffiliatePage } from './track-affiliate';

@NgModule({
  declarations: [
    TrackAffiliatePage,
  ],
  imports: [
    IonicPageModule.forChild(TrackAffiliatePage),
  ],
})
export class TrackAffiliatePageModule {}
