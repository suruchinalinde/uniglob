import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepositNetellerPage } from './deposit-neteller';

@NgModule({
  declarations: [
    DepositNetellerPage,
  ],
  imports: [
    IonicPageModule.forChild(DepositNetellerPage),
  ],
})
export class DepositNetellerPageModule {}
