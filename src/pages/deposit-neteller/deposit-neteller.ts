import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,LoadingController,Loading} from 'ionic-angular';
import { HomePage } from '../home/home';
import { DepositPage } from '../deposit/deposit';
import {CurrencyService} from '../../providers/currency-service';
import {  FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {Network} from '@ionic-native/network';
import { LoginPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-deposit-neteller',
  templateUrl: 'deposit-neteller.html',
})
export class DepositNetellerPage {
  flag: boolean;
  currencys:any;
  main:any;
loading:Loading;
formgroup:FormGroup;
get_rser:any;

account:AbstractControl;
amount:AbstractControl;
currency:AbstractControl;
sercureId:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams,public currencyserv:CurrencyService, 
    public formbuilder:FormBuilder , public serv:AuthServiceProvider,
    public alertCtrl:AlertController, public loadingCtrl:LoadingController,
     public network :Network) {
this.currencys=this.currencyserv.currency;
console.log(this.currencys);
this.main=this.navParams.get('datas');



this.formgroup = formbuilder.group({
  account:['',Validators.required],
  amount:['',Validators.required],
  currency:['',Validators.required],
  sercureId:['',Validators.required],



});

// newComment = new FormControl('', [validateEmptyField]);

this.account= this.formgroup.contains['account'];
this.amount= this.formgroup.contains['amount'];
this.currency= this.formgroup.contains['currency'];
this.sercureId= this.formgroup.contains['sercureId'];




  }

  homefun13(){
    this.navCtrl.setRoot(DepositPage,{icon:this.main});
  }
  NetellerFun(accountid,amount,currency,code){
    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: [{
              text:'Ok',
              handler:()=>{
                this.navCtrl.setRoot(LoginPage);
              }
            }]
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      // this.navCtrl.setRoot(LoginPage);
      return;
    }

    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: ' Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1000
  
    });
    this.loading.present();
  
    

    this.serv.Netellerfunction(accountid,amount,currency,code).subscribe(data=>{
      console.log(data);
     
      this.get_rser = data;
      console.log(this.get_rser);
    
   
    // () => {

      if (this.get_rser.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:  this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.get_rser.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:  this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.get_rser.status == "success") {

        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(DepositPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
      if (this.get_rser.status == "error") {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Failed',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(DepositPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }

    },error =>{
      this.loading.dismiss();

      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    //  this.splash.showError(error),
    },
  )
}
  }
  validationfun(amount){
    console.log(amount);
    if(amount >9){
      console.log('successfully');
      this.flag=false;
    }
    else{
    this.flag=true;
    }
  }
  
}
