import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController,LoadingController, Loading, Platform} from 'ionic-angular';
import {AuthServiceProvider, User} from '../../providers/auth-service/auth-service'
import { HomePage } from '../home/home';
import { Token } from '@angular/compiler';
import { PersonalInfoPage } from '../personal-info/personal-info';
import { SendFeedbackPage } from '../send-feedback/send-feedback';
import {Network} from '@ionic-native/network';
import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import { subscribeOn } from 'rxjs/operator/subscribeOn';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
// client

email:any="coolkatty271@gmail.com";
password:any="123456";
usertype:any="user";

// partner

// email:any="parmitabhattacharya@ymail.com";
// password:any="abc123";
// usertype:any="agent";


// email:any="j_basi@hotmail.co.uk";
// password:any="test123";
// usertype:any="affiliate";



loading:Loading;
alert:any;

formgroup:FormGroup;
emaile:AbstractControl;
passworde:AbstractControl;
usertypee:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams,public serv:AuthServiceProvider,
  public alertCtrl:AlertController, public loadingCtrl:LoadingController , public network:Network, 
  public platform:Platform, public formbuilder:FormBuilder) {
    // this.login();
   if(network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: ' Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
}

this.formgroup = formbuilder.group({
  emaile:['',Validators.required],
  passworde:['',Validators.required],
  usertypee:[Validators.required]
});

this.emaile= this.formgroup.contains['emaile'];
this.passworde= this.formgroup.contains['passworde'];
this.usertypee= this.formgroup.contains['usertypee'];




  }

 

login(email,password,usertype){

  if(this.network.type === 'none')
  {
let alert = this.alertCtrl.create({
  title: 'Sorry ! No Internet Connection ',
  subTitle:'Please Connect To Internet And Try Again ',
  buttons: ['ok']
});
alert.present();
}
else{

  let loading = this.loadingCtrl.create({
    content: "Please wait...",

  });
  loading.present();

this.serv.loginfun(this.email,this.password,this.usertype).subscribe(data =>{
  console.log(data);


  if(data.status !=="error"){
    loading.dismiss();
        this.serv.setUser(data);
      
        this.serv.setAuthToken(data);
        
        this.navCtrl.setRoot(HomePage);
    
      }
      else{
        loading.dismiss();
    
        let alert = this.alertCtrl.create({
       
          subTitle: data.message,
          buttons: ['OK'] 
        });
        alert.present();
      }


});
  }
}

forgetpasswordFun(){
  this.navCtrl.setRoot(ForgetpasswordPage);
}
  
}
