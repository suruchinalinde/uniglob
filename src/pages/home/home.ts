import { Component } from '@angular/core';
import { NavController, Loading } from 'ionic-angular';
import { PersonalInfoPage } from '../personal-info/personal-info';
import { FundingPage } from '../funding/funding';
import { SendFeedbackPage } from '../send-feedback/send-feedback';
import { ChangePasswordPage } from '../change-password/change-password';
import { CallBackPage } from '../call-back/call-back';
import { AccountInfoPage } from '../account-info/account-info';
import { InternalTransgerRequestPage } from '../internal-transger-request/internal-transger-request';
import { RequestNewAccountPage } from '../request-new-account/request-new-account';
import { DownloadFilePage } from '../download-file/download-file';
import {LoadingController, AlertController, ToastController} from 'ionic-angular';
import { PersonalInfoService } from '../../providers/personal-info-service';
import {Network} from '@ionic-native/network';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { DocumentPage } from '../document/document';
import { Clipboard } from '@ionic-native/clipboard';
import { PersonalPPage } from '../personal-p/personal-p';
import { PersonalAPage } from '../personal-a/personal-a';
import { MyclientPage } from '../myclient/myclient';
import { TrackAffiliatePage } from '../track-affiliate/track-affiliate';
import { AffiliateMyClientPage } from '../affiliate-my-client/affiliate-my-client';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  visitors: any;
  clients: any;
  affiliatecode: any;
  link: string;
  mm: string;
  user_type:any;
loading:Loading;
document_verify:any;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, 
    public personalInfoserv:PersonalInfoService , public network:Network, public alertCtrl:AlertController,
  public serv:AuthServiceProvider, public clipboard:Clipboard, public toastCtrl:ToastController) {
    // this.trackaffiliateFunction();
    console.log(this.serv.currentUser.affiliatecode);

    this.affiliatecode=this.serv.currentUser.affiliatecode;
    this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliatecode;
    this.user_type=this.serv.currentUser.role;
    this.document_verify=this.serv.currentUser.document_verify;
  this.personalinfoFun();
    }


    documentfun(){
      this.navCtrl.setRoot(DocumentPage);
    }
    
  callpage1(){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: ' Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    
    this.navCtrl.setRoot(PersonalInfoPage);
  }
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  callpage2(){
    // let loader = this.loadingCtrl.create({
    //   content: "Please wait...",
    //   duration: 1000
    // });
    // loader.present();
    this.navCtrl.setRoot(FundingPage);
  }
  callpage3(){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: ' Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.navCtrl.setRoot(SendFeedbackPage);
  }
}

  callpage4(){
    this.navCtrl.setRoot(ChangePasswordPage);
  }
  callpage5(){
    this.navCtrl.setRoot(CallBackPage);
  }
  callpage6(){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.navCtrl.setRoot(AccountInfoPage);
  }
}

  callpage7(){
    this.navCtrl.setRoot(InternalTransgerRequestPage);
  }

  callpage8(){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.navCtrl.setRoot(RequestNewAccountPage);
  }
  }

  callpage9(){
    this.navCtrl.setRoot(DownloadFilePage);
  }
  personalinfoFun(){
    console.log(this.user_type);
    this.personalInfoserv.get_personal_info(this.user_type).subscribe(response=>{
      console.log(response);
      // console.log(response.user.role);
      // this.affiliatecode=response.user.affiliatecode;
            // console.log(this.affiliatecode);


      this.personalInfoserv.setUser(response);
      
    })
  }
  ngIfCtrl(){
    // var hide = !this.hide;
    // let alrt =this.alertCtrl.create({
    //   title:'Link Copied  Successfully ',
    //   subTitle:this.mm,
    //   buttons: ['OK']
        
    //   })
    //   alrt.present();
    let toast = this.toastCtrl.create({
      message: 'Link Copied  Successfully ',
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
    //  this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliate;
     this.link=this.mm;
     console.log(this.link);
   
     this.clipboard.copy(this.link);
    
     
     this.clipboard.paste().then(
      (resolve: string) => {
      
       },
       (reject: string) => {
        //  alert('Error: ' + reject);
       }
     );
  
   }

// partner Home Page

callpage11(){
  if(this.network.type === 'none')
{
let alert = this.alertCtrl.create({
title: 'Sorry ! No Internet Connection ',
subTitle:'Please Connect To Internet And Try Again ',
buttons: ['ok']
});
alert.present();
}
else{

this.navCtrl.setRoot(PersonalPPage);
}

}

callpageFun(){
  if(this.network.type === 'none')
  {
  let alert = this.alertCtrl.create({
  title: 'Sorry ! No Internet Connection ',
  subTitle:'Please Connect To Internet And Try Again ',
  buttons: ['ok']
  });
  alert.present();
  }
  else{
  
  this.navCtrl.setRoot(PersonalAPage);
  }
  
}
callpage0()
{
  this.navCtrl.setRoot(MyclientPage)
} 


// trackaffiliateFunction(){
 
// }

trackFun(){
  if(this.network.type === 'none')
  {
  let alert = this.alertCtrl.create({
  title: 'Sorry ! No Internet Connection ',
  subTitle:'Please Connect To Internet And Try Again ',
  buttons: ['ok']
  });
  alert.present();
  }
  else{
  
  this.navCtrl.setRoot(PersonalPPage);
  }

  this.navCtrl.setRoot(TrackAffiliatePage)
}

AffiliateMyClient(){
  this.navCtrl.setRoot(AffiliateMyClientPage);
}
logout(){

  let alert = this.alertCtrl.create({
    title:'Logout',
    subTitle: 'Are you sure you want to logout from this app ?',
    buttons: [
      {
    text:'NO',
    role: 'cancel',
    },
    {
    text:'YES',
    handler:()=>{

      this.serv.localstorge=null;
      this.navCtrl.setRoot(LoginPage);

      
      let toast = this.toastCtrl.create({
        message:'Logout successfully',
        duration: 2000,
        position: 'bottom'
      });
    
      toast.onDidDismiss(() => {
      
        console.log('Dismissed toast');
        
      });
    
      toast.present();
    }
    }

  ]
    });
    alert.present();
}
}
