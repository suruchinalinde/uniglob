import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ActionSheetController, AlertController,LoadingController,Loading} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html',
})
export class ForgetpasswordPage {

  get_response: any;
  email:AbstractControl;
  type:AbstractControl;
  formgroup:FormGroup;
loading:Loading;


  constructor(public navCtrl: NavController, public navParams: NavParams, public formbuilder:FormBuilder,
  public serv:AuthServiceProvider,public alertCtrl:AlertController, public loadingCtrl:LoadingController) {
    this.formgroup = formbuilder.group({
      email:['',Validators.required],
      type:[Validators.required],
      // usertypee:[Validators.required]
    });
    
    this.email= this.formgroup.contains['email'];
    this.type= this.formgroup.contains['type'];
    // this.usertypee= this.formgroup.contains['usertypee'];
  }

  loginfun(){
    this.navCtrl.setRoot(LoginPage);
  }

  forgetfun(UserEmail,user_type){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration: 1000
    });
    this.loading.present();

    this.serv.ForgetPassFun(UserEmail,user_type).subscribe(data=>{
      console.log(data);

this.get_response=data;
    },
    error => {
          this.loading.dismiss();
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'error',
        buttons: ['OK']
      });
      alert.present();
            // this.splash.showError(error),
          },  
          () => {

            if (this.get_response.status == 'success') {
              this.loading.dismiss();

              let alert = this.alertCtrl.create({
                subTitle: this.get_response.msg,
                buttons: ['OK']
              });
              alert.present();
            }
            else {
           this.loading.dismiss();
              let alert = this.alertCtrl.create({
                subTitle: this.get_response.msg,
                buttons: ['OK']
              });
              alert.present();
            }
          }
  )
  }

}
