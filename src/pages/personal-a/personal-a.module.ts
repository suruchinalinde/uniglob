import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PersonalAPage } from './personal-a';

@NgModule({
  declarations: [
    PersonalAPage,
  ],
  imports: [
    IonicPageModule.forChild(PersonalAPage),
  ],
})
export class PersonalAPageModule {}
