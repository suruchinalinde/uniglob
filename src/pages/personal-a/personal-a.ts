import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';
import { AlertController, LoadingController, Loading ,ToastController} from 'ionic-angular';
import { PersonalInfoService } from '../../providers/personal-info-service';
import { LoginPage } from '../login/login';
import { Clipboard } from '@ionic-native/clipboard';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
/**
 * Generated class for the PersonalAPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-personal-a',
  templateUrl: 'personal-a.html',
})
export class PersonalAPage {

  affiliate: string;
  link: string;
  mm: string;
  hide: any=false;
  save_response: any;
loading:Loading;
  get_response: any;
  user_type:string;
  items:any;

  personalInfo = {
    firstName:'',
    lastName:'',
    phoneNumber:'',
 
  };

  addressDetails = {
    address:'',
    country:'',
    UserEmail:'',
    business_info:'',
    ib_exp:'',
    estimateno:'',
    primarymethod:'',
    other_method:'',
    countrycode:'',
  };
  formgroup: FormGroup;

  firstName:AbstractControl;
  lastName:AbstractControl;
  phoneNumber:AbstractControl;
  address:AbstractControl;
  country:AbstractControl;
  UserEmail:AbstractControl;
  business_info:AbstractControl;
  ib_exp:AbstractControl;
  estimateno:AbstractControl;
  primarymethod:AbstractControl;
  other_method:AbstractControl;
  countrycode:AbstractControl;




  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl:LoadingController,  public formbuilder:FormBuilder,public serv:AuthServiceProvider,
  public personalInfoService:PersonalInfoService, public alertCtrl:AlertController,public network:Network,
  private clipboard: Clipboard,public toastCtrl:ToastController) {
    this.affiliate=this.serv.currentUser.affiliatecode;
    this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliate;

    this.user_type=this.serv.currentUser.role;
// console.log(this.personalInfoService.getinfo.primarymethod);
    this.GetPersoalInfo();


    this.formgroup = formbuilder.group({

      firstName:['',Validators.required],
      lastName:['',Validators.required],
      phoneNumber:['',Validators.required],
      address:['',Validators.required],
      country:['',Validators.required],
      UserEmail:['',Validators.required],
      business_info:['',Validators.required],
      ib_exp:['',Validators.required],
      estimateno:['',Validators.required],
      primarymethod:['',Validators.required],
      other_method:['',Validators.required],
      countrycode:['',Validators.required],
    
    
    });
    
    
    this.firstName=this.formgroup.contains['firstName'];
      this.lastName=this.formgroup.contains['lastName'];
      this.phoneNumber=this.formgroup.contains['phoneNumber'];
      this.address=this.formgroup.contains['address'];
      this.country=this.formgroup.contains['country'];
      this.UserEmail=this.formgroup.contains['UserEmail'];
      this.business_info=this.formgroup.contains['business_info'];
      this.ib_exp=this.formgroup.contains['ib_exp'];
      this.estimateno=this.formgroup.contains['estimateno'];
      this.primarymethod=this.formgroup.contains['primarymethod'];
      this.other_method=this.formgroup.contains['other_method'];
      this.countrycode=this.formgroup.contains['countrycode'];
    

    
  }
 
  homefun66(){
    this.navCtrl.setRoot(HomePage);
  }

  GetPersoalInfo(){


    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['OK']
      });
      alert.present();
      this.navCtrl.setRoot(LoginPage);
      return;
    }

   
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      duration:2000
    });
    this.loading.present();

    // console.log(this.user_type);
this.personalInfoService.get_personal_info(this.user_type).subscribe(response=>{
// this.loading.dismiss();
this.personalInfoService.setUser(response);

  console.log(response);
  this.get_response = response;
},
error => {
  // this.splash.showError(error);
  this.loading.dismiss();

  let alert = this.alertCtrl.create({
    // title: 'Error',
    subTitle: 'error',
    buttons: ['OK']
  });
  alert.present();
},

() => {

  if (this.get_response.status == 'T_EMPTY') {
    this.loading.dismiss();

    // this.splash.showError("You are logged out. Please Login again.");
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: this.save_response.msg,
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(LoginPage);
    return;
  }
  else if (this.get_response.status == 'T_INVAL') {
    this.loading.dismiss();

    // this.splash.showError("You are logged out. Please Login again.");
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: this.save_response.msg,
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.setRoot(LoginPage);
    return;
  }

  // populate the user array. 
  this.personalInfoService.populateUserInfo(this.get_response);
  this.personalInfo = this.personalInfoService.userInfo.personalInfo;
  this.addressDetails = this.personalInfoService.userInfo.addressDetails;
  this.loading.dismiss();
})
}
isReadonly() {
  return this.isReadonly;   //return true/false 
}


PostPersonaInfo()
{ 
  if (this.serv.is_user_authenticated() == false) {
  let alert = this.alertCtrl.create({
    // title: 'Error',
    subTitle: 'You are logged out. Please Login again.',
    buttons: [{
          text:'ok',
          handler:()=>{
            this.navCtrl.setRoot(LoginPage);
          }
        }]
  });
  alert.present();
  // this.splash.showError("You are logged out. Please Login again.");
  // this.navCtrl.setRoot(LoginPage);
  return;
}

if(this.network.type === 'none')
{
let alert = this.alertCtrl.create({
title: 'Sorry ! No Internet Connection ',
subTitle:'Please Connect To Internet And Try Again ',
buttons: ['ok']
});
alert.present();
}
else{


  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    // duration:1500

  });
  this.loading.present();
  // this.splash.showLoading();
  
  console.log(this.personalInfo, this.addressDetails);
  this.personalInfoService.save_personal_info(this.personalInfo, this.addressDetails ,this.user_type)
    .subscribe(response => {
      console.log(response);
      // this.loading.dismiss();
// this.affiliate=response.user.affiliatecode;
      this.save_response = response;
      // console.log(response);
 
   
    // () => {
      // this.loading.dismiss();

      if (this.save_response.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.save_response.msg,
          buttons: [{
            text:'ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.save_response.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.save_response.msg,
          buttons: [{
            text:'ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.save_response.status == "success") {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.save_response.message,
          buttons: [{
            text:'ok',
            // handler:()=>{
            //   this.navCtrl.setRoot(PersonalInfoPage);
            // }
          }]
        });
        alert.present();
     
      }
      // else {
      //   let alert = this.alertCtrl.create({
      //     title: 'Error',
      //     subTitle: 'Error! Information could not be saved',
      //     buttons: ['OK']
      //   });
      //   alert.present();
      //   // this.splash.showError("Error! Information could not be saved.");
      // }

    // }
    }, error =>{
      this.loading.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'error',
        buttons: ['OK']
      });
      alert.present();
    //  this.splash.showError(error),
    },
  // console.log(this.personalInfo,this.addressDetails);
  // console.log(this.personalInfo,this.addressDetails);
  // this.personalInfoService.save_personal_info(this.personalInfo,this.addressDetails).subscribe(data=>{
  //   if(data.status=='success'){

  //   }
  // })
  )
}
}

datafun(hide){
  hide=false;
}

ngIfCtrl(){
  // var hide = !this.hide;
  // let alrt =this.alertCtrl.create({
  //   title:'Link Copied  Successfully ',
  //   subTitle:this.mm,
  //   buttons: ['OK']
      
  //   })
  //   alrt.present();
  let toast = this.toastCtrl.create({
    message: 'Link Copied  Successfully ',
    duration: 1000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
  //  this.mm="https://www.uniglobemarkets.com/register/?rl="+this.affiliate;
   this.link=this.mm;
   console.log(this.link);
 
   this.clipboard.copy(this.link);
  
   
   this.clipboard.paste().then(
    (resolve: string) => {
    
     },
     (reject: string) => {
      //  alert('Error: ' + reject);
     }
   );

 }

 notify(){
  console.log(this.addressDetails.country);
// this.items=this.addressDetails.country;
// this.names.code = this.addressDetails.country;

 switch(this.addressDetails.country)
{
  case "AF" :   this.items='+93';
                      break;

                      case "AF" :   this.items='+93';
                      break;
                      case "AL" :   this.items='+355';
                      break;
                      case "DZ" :   this.items='+213';
                      break;
                      case "AS" :   this.items='+1-684';
                      break;
                      case  "AD":   this.items='+376';
                      break;
                      case "AO" :   this.items='+244';
                      break;
                      case "AI" :   this.items='+1-268';
                      break;
                      case "AQ" :   this.items='+672';
                      break;
                      case "AG" :   this.items='+1-268';
                      break;
                      case "AR" :   this.items='+54';
                      break;
                      case "AM" :   this.items='+374';
                      break;
                      case "AW" :   this.items='+297';
                      break;
                      case "AU" :   this.items='+61';
                      break;
                      case "AT" :   this.items='+43';
                      break;
                      case "AZ" :   this.items='+994';
                      break;
                      case "BS" :   this.items='+1-242';
                      break;
                      case "BH" :   this.items='+973';
                      break;
                      case "BD" :   this.items='+880';
                      break;
                      case "BB" :   this.items='+1-246';
                      break;
                      case "BY" :   this.items='+375';
                      break;
                      case "BE" :   this.items='+32';
                      break;
                      case "BZ" :   this.items='+501';
                      break;
                      case "BJ" :   this.items='+229';
                      break;
                      case "BM" :   this.items='+1-441';
                      break;
                      case "BT" :   this.items='+975';
                      break;
                      case "BO" :   this.items='+591';
                      break;
                      case "BA" :   this.items='+387';
                      break;
                      case "BW" :   this.items='+267';
                      break;
                      case "BV" :   this.items='+47';
                      break;
                      case "BR" :   this.items='+55';
                      break;
                      case "IO" :   this.items='+246';
                      break;
                      case "BN" :   this.items='+673';
                      break;
                      case "BG" :   this.items='+359';
                      break;
                      case "BF" :   this.items='+226';
                      break;
                      case "BI" :   this.items='+257';
                      break;
                      case "KH" :   this.items='+855';
                      break;
                      case "CM" :   this.items='+237';
                      break;
                      case "CA" :   this.items='+1';
                      break;
                      case "CV" :   this.items='+238';
                      break;
                      case "KY" :   this.items='+1-345';
                      break;
                      case "CF" :   this.items='+236';
                      break;
                      case "TD" :   this.items='+235';
                      break;
                      case "CL" :   this.items='+56';
                      break;
                      case "CN" :   this.items='+86';
                      break;
                      case "CX" :   this.items='+61';
                      break;
                      case "CC" :   this.items='+61';
                      break;
                      case "CO" :   this.items='+57';
                      break;
                      case "KM" :   this.items='+269';
                      break;
                      case "CG" :   this.items='+242';
                      break;
                      case "CK" :   this.items='+682';
                      break;
                      case "CR" :   this.items='+506';
                      break;
                      case "CI" :   this.items='+225';
                      break;
                      case "HR" :   this.items='+385';
                      break;
                      case "CU" :   this.items='+53';
                      break;
                      case  "CY" :   this.items='+357';
                      break;
                      case "CZ" :   this.items='+420';
                      break;
                      case "DK" :   this.items='+45';
                      break;
                      case "DJ" :   this.items='+253';
                      break;
                      case "DM" :   this.items='+1-767';
                      break;
                      case "DO" :   this.items='+1-809';
                      break;
                      case "TL" :   this.items='+670';
                      break;
                      case "EC" :   this.items='+593';
                      break;
                      case "EG" :   this.items='+20';
                      break;
                      case "SV" :   this.items='+503';
                      break;
                      case "GQ" :   this.items='+240';
                      break;
                      case "ER" :   this.items='+291';
                      break;
                      case "EE" :   this.items='+372';
                      break;
                      case "ET" :   this.items='+251';
                      break;
                      case "FK" :   this.items='+500';
                      break;
                      case "FO" :   this.items='+298';
                      break;
                      case "FJ" :   this.items='679';
                      break;
                      case "FI" :   this.items='+358';
                      break;
                      case "FR" :   this.items='+33';
                      break;
                      case "GF" :   this.items='+689';
                      break;
                      case "PF" :   this.items='+689';
                      break;
                      case "TF" :   this.items='+';
                      break;
                      case "GA" :   this.items='+241';
                      break;
                      case "GM" :   this.items='+220';
                      break;
                      case "GE" :   this.items='+995';
                      break;
                      case "DE" :   this.items='+49';
                      break;
                      case "GH" :   this.items='+233';
                      break;
                      case "GI" :   this.items='+350';
                      break;
                      case "GR" :   this.items='30';
                      break;
                      case "GL" :   this.items='+299';
                      break;
                      case "GD" :   this.items='1-473';
                      break;
                      case "GP" :   this.items='+590';
                      break;
                      case "GU" :   this.items='+1-473';
                      break;
                      case "GT" :   this.items='+502';
                      break;
                      case "GN" :   this.items='+224';
                      break;
                      case "GW" :   this.items='+245';
                      break;
                      case "GY" :   this.items='+592';
                      break;
                      case "HT" :   this.items='+509';
                      break;
                      case "HM" :   this.items='+011';
                      break;
                      case "HN" :   this.items='+504';
                      break;
                      case "HK" :   this.items='+852';
                      break;
                      case "HU" :   this.items='+36';
                      break;
                      case "IS" :   this.items='+354';
                      break;
                      case "IN" :   this.items='+91';
                      break;
                      case "ID" :   this.items='+62';
                      break;
                      case "IR" :   this.items='+98';
                      break;
                      case "IQ" :   this.items='+964';
                      break;
                      case "IE" :   this.items='+353';
                      break;
                      case "IL" :   this.items='+972';
                      break;
                      case "IT" :   this.items='+39';
                      break;
                      case "JM" :   this.items='+1-876';
                      break;
                      case "JP" :   this.items='+81';
                      break;
                      case "JO" :   this.items='+962';
                      break;
                      case "KZ" :   this.items='+7';
                      break;
                      case "KE" :   this.items='+254';
                      break;
                      case "KI" :   this.items='+686';
                      break;
                      case "KP" :   this.items='850';
                      break;
                      case "KR" :   this.items='+82';
                      break;
                      case "KW" :   this.items='+965';
                      break;
                      case "KG" :   this.items='+996';
                      break;
                      case "LA" :   this.items='+996';
                      break;
                      case "LV" :   this.items='+996';
                      break;
                      case "LB" :   this.items='+961';
                      break;
                      case "LS" :   this.items='+266';
                      break;
                      case "LR" :   this.items='+231';
                      break;
                      case "LY" :   this.items='+218';
                      break;
                      case "LI" :   this.items='+423';
                      break;
                      case "LT" :   this.items='+370';
                      break;
                      case "LU" :   this.items='+352';
                      break;
                      case "MO" :   this.items='+853';
                      break;
                      case "MK" :   this.items='+389';
                      break;
                      case "MG" :   this.items='+261';
                      break;
                      case "MW" :   this.items='+265';
                      break;
                      case "MY" :   this.items='+60';
                      break;
                      case "MV" :   this.items='+960';
                      break;
                      case "ML" :   this.items='+223';
                      break;
                      case "MT" :   this.items='+356';
                      break;
                      case "MH" :   this.items='+692';
                      break;
                      case "MQ" :   this.items='+596';
                      break;
                      case "MR" :   this.items='+222';
                      break;
                      case "MU" :   this.items='+230';
                      break;
                      case "YT" :   this.items='+262';
                      break;
                      case "MX" :   this.items='+52';
                      break;
                      case "FM" :   this.items='+691';
                      break;
                      case "MD" :   this.items='+373';
                      break;
                      case "MC" :   this.items='+377';
                      break;
                      case "MN" :   this.items='+976';
                      break;
                      case "MS" :   this.items='+1-664';
                      break;
                      case "MA" :   this.items='+212';
                      break;
                      case "MZ" :   this.items='+258';
                      break;
                      case "MM" :   this.items='+95';
                      break;
                      case "NA" :   this.items='+264';
                      break;
                      case "NR" :   this.items='+674';
                      break;
                      case "NP" :   this.items='+977';
                      break;
                      case "NL" :   this.items='+31';
                      break;
                      case "AN" :   this.items='+599';
                      break;
                      case "NC" :   this.items='+687';
                      break;
                      case "NZ" :   this.items='+64';
                      break;
                      case "NI" :   this.items='+505';
                      break;
                      case "NE" :   this.items='+227';
                      break;
                      case "NG" :   this.items='+234';
                      break;
                      case "NU" :   this.items='+683';
                      break;
                      case "NF" :   this.items='+672';
                      break;
                      case "MP" :   this.items='+1-670';
                      break;
                      case "NO" :   this.items='+47';
                      break;
                      case "OM" :   this.items='+968';
                      break;
                      case "PK" :   this.items='+92';
                      break;
                      case "PW" :   this.items='+680';
                      break;
                      case "PA" :   this.items='+507';
                      break;
                      case "PG" :   this.items='+675';
                      break;
                      case "PY" :   this.items='+595';
                      break;
                      case "PE" :   this.items='+51';
                      break;
                      case "PH" :   this.items='+63';
                      break;
                      case "PN" :   this.items='+64';
                      break;
                      case "PL" :   this.items='+48';
                      break;
                      case "PT" :   this.items='+351';
                      break;
                      case "PR" :   this.items='+1-787';
                      break;
                      case "QA" :   this.items='+974';
                      break;
                      case "RE" :   this.items='+262';
                      break;
                      case "RO" :   this.items='+40';
                      break;
                      case "RU" :   this.items='+7';
                      break;
                      case "RW" :   this.items='+250';
                      break;
                      case "KN" :   this.items='+1-869';
                      break;
                      case "LC" :   this.items='+1-758';
                      break;
                      case "VC" :   this.items='+1-784';
                      break;
                      case "WS" :   this.items='+685';
                      break;
                      case "SM" :   this.items='+374';
                      break;
                      case "ST" :   this.items='+239';
                      break;
                      case "SA" :   this.items='+966';
                      break;
                      case "SN" :   this.items='+221';
                      break;
                      case "SC" :   this.items='+248';
                      break;
                      case "SL" :   this.items='+232';
                      break;
                      case "SG" :   this.items='+65';
                      break;
                      case "SK" :   this.items='+421';
                      break;
                      case "SI" :   this.items='+386';
                      break;
                      case "SB" :   this.items='+677';
                      break;
                      case "SO" :   this.items='+252';
                      break;
                      case "ZA" :   this.items='+27';
                      break;
                      case "GS" :   this.items="+500";
                      break;
                      case "ES" :   this.items='+34';
                      break;
                      case "LK" :   this.items='+94';
                      break;
                      case "SH" :   this.items='+290';
                      break;
                      case "PM" :   this.items='+508';
                      break;
                      case "SD" :   this.items='+249';
                      break;
                      case "SR" :   this.items='+597';
                      break;
                      case "SJ" :   this.items='+47';
                      break;
                      case "SZ" :   this.items='+268';
                      break;
                      case "SE" :   this.items='+46';
                      break;
                      case "CH" :   this.items='+41';
                      break;
                      case "SY" :   this.items='+963';
                      break;
                      case "TW" :   this.items='+886';
                      break;
                      case "TJ" :   this.items='+992';
                      break;
                      case "TZ" :   this.items='+255';
                      break;
                      case "TH" :   this.items='+66';
                      break;
                      case "TG" :   this.items='+228';
                      break;
                      case "TK" :   this.items='+690';
                      break;
                      case "TO" :   this.items='+676';
                      break;
                      case "TT" :   this.items='+1-868';
                      break;
                      case "TN" :   this.items='+216';
                      break;
                      case "TR" :   this.items='+90';
                      break;
                      case "TM" :   this.items='+993';
                      break;
                      case "TC" :   this.items='+1-649';
                      break;
                      case "TV" :   this.items='+688';
                      break;
                      case "UG" :   this.items='+256';
                      break;
                      case "UA" :   this.items='+380';
                      break;
                      case "AE" :   this.items='+971';
                      break;
                      case "GB" :   this.items='+44';
                      break;
                      case "US" :   this.items='+1';
                      break;
                      case "UM" :   this.items='+264';
                      break;
                      case "UY" :   this.items='+598';
                      break;
                      case "UZ" :   this.items='+998';
                      break;
                      case "VU" :   this.items='+678';
                      break;
                      case "VA" :   this.items='+379';
                      break;
                      case "VE" :   this.items='+58';
                      break;
                      case "VN" :   this.items='+84';
                      break;
                      case "VG" :   this.items='+1-284';
                      break;
                      case "VI" :   this.items='+1-340';
                      break;
                      case "WF" :   this.items='+681';
                      break;
                      case "EH" :   this.items='+212';
                      break;
                      case "YE" :   this.items='+967';
                      break;
                      case "RS" :   this.items='+381';
                      break;
                      case "ZM" :   this.items='+260';
                      break;
                      case "ZW" :   this.items='+263';
                      break;
                      case "AX" :   this.items='+358';
                      break;
                      case "PS" :   this.items='+970';
                      break;
                      case "ME" :   this.items='+382';
                      break;
                      case "GG" :   this.items='+44-1481';
                      break;
                      case "IM" :   this.items='+44-1624';
                      break;
                      case "JE" :   this.items='+44-1534';
                      break;
                      case "CM" :   this.items='+599';
                      break;
                      case "CI" :   this.items='+225';
                      break;
                      case "XK" :   this.items='+383';
                      break;


}
 }

}
