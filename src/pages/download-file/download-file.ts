import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { PersonalInfoPage } from '../personal-info/personal-info';

/**
 * Generated class for the DownloadFilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-download-file',
  templateUrl: 'download-file.html',
})
export class DownloadFilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  homefun3(){
    this.navCtrl.setRoot(HomePage);
  }

}
