import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Form } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DepositPage } from '../deposit/deposit';
import { WithdrawalPage } from '../withdrawal/withdrawal';
 import {LoadingController,AlertController,Loading} from 'ionic-angular';
 import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
 import {Network} from '@ionic-native/network';
/**
 * Generated class for the FundingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-funding',
  templateUrl: 'funding.html',
})
export class FundingPage {
loading:Loading;
name:any;
icon:any;
main:any;
tranferinfo:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl:LoadingController,
  public serv:AuthServiceProvider, public network:Network , public alertCtrl:AlertController) {
    // this.Imagefun()
  //  this.GettransactionINFoFun();
  }
  // Imagefun(){
  //   // this.serv.getEwallet().subscribe(res=>{
  //   //   console.log(res.accounts);
  //   //   this.name=res.accounts;
  //   //   // this.main=res;
  //   //   // this.name=res.accounts.name;
  //   //   // this.icon=res.accounts.icon;
  //   //   this.navCtrl.setRoot(WithdrawalPage,{name:name})
     

  //   // })
  // }

  homefun4(){
    this.navCtrl.setRoot(HomePage);
  }

  GettransactionINFoFun(){
    this.serv.GetTransactionInfoFun().subscribe(data=>{
      console.log(data);
this.tranferinfo=data.history[0];
    })
  }

  depositfun(){

    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: ' Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
  
    });
    loader.present();

    this.serv.getEwallet().subscribe(res=>{
      console.log(res);
      this.icon=res.accounts;
      // this.main=res;
      // this.name=res.accounts.name;
      // this.icon=res.accounts.icon;
      this.navCtrl.setRoot(DepositPage,{icon:this.icon,transferinfo:this.tranferinfo})
      loader.dismiss();
    })
  }
    // this.navCtrl.setRoot(DepositPage);
  }


  withdrawalfun(){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 1000
    });
    loader.present();

    this.serv.getEwallet().subscribe(res=>{
      console.log(res);
      this.icon=res.accounts;
      // this.main=res;
      // this.name=res.accounts.name;
      // this.icon=res.accounts.icon;
      this.navCtrl.setRoot(WithdrawalPage,{icon:this.icon})
     loader.dismiss();
  
    })
// this.Imagefunn();
    // this.navCtrl.setRoot(WithdrawalPage);
  }
  }
  


}
