import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FundingPage } from '../funding/funding';

import { WdcashuPage } from '../wdcashu/wdcashu';
import { VisawithdrawalPage } from '../visawithdrawal/visawithdrawal';
import {FeedbackService} from '../../providers/feedback-service';
import {AlertController,LoadingController,Loading} from 'ionic-angular';
import { HomePage } from '../home/home';
import { BankwithdrwalPage } from '../bankwithdrwal/bankwithdrwal';
import { DepositVisaPage } from '../deposit-visa/deposit-visa';
import { DepositSkrillPage } from '../deposit-skrill/deposit-skrill';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {Network} from '@ionic-native/network';
// import { PartnerHomePage } from '../partner-home/partner-home';
// import { PartnerFundingPage } from '../partner-funding/partner-funding';

@IonicPage()
@Component({
  selector: 'page-withdrawal',
  templateUrl: 'withdrawal.html',
})
export class WithdrawalPage {
  get_response:any;
  loading:Loading;
  account:any;
  datas:any;
  icon:any;
  main:any;
 user_type:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public feedbackserv:FeedbackService,
    public alertCtrl:AlertController, public loadingCtrl:LoadingController,public serv:AuthServiceProvider,
  public network:Network) {

    this.user_type=this.serv.currentUser.role;
    this.accountfun();
    // this.Imagefun();
    this.datas=this.navParams.get('icon');
// this.data=this.navParams.get('datas');
    console.log(this.datas);
    // this.fund.Imagefunn();
  }

//   Imagefun(){
//     this.serv.getEwallet().subscribe(res=>{
     
// this.datas=res.accounts;
// this.main=res;

// console.log(this.main);
//     })
//   }

  imgMainfun(name){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{

    console.log(name);
    switch (name)
    {
      case "CashU":     this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

     case "Neteller":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

    case "Skrill":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

    case "Paypal":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

    case "Qiwi":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break; 

    case "WebMoney":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

   case "OKPay":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

  case "Perfect Money":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

 case "Wirecapital":   this.navCtrl.setRoot(WdcashuPage,{name:name,datas:this.datas});
                        break;

    }
  }

  }

  accountfun(){

      // this.loading = this.loadingCtrl.create({
      //   content: 'Please wait...',
      //   duration: 500
      // });
      // this.loading.present();
  
      this.feedbackserv.get_accounts().subscribe(data=>{
    
        this.feedbackserv.setAcountId(data);
            this.get_response=data;

      })
    }

  homefun16(){
    this.navCtrl.setRoot(FundingPage);
  

  }

wdnewvisa(){
  this.navCtrl.setRoot(VisawithdrawalPage,{datas:this.datas});
}
bankwith(){
  this.navCtrl.setRoot(VisawithdrawalPage,{datas:this.datas});
}
}
