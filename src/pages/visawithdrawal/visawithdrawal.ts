import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { WithdrawalPage } from '../withdrawal/withdrawal';
import {FeedbackService} from '../../providers/feedback-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import {AlertController,Loading,LoadingController} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { WdcashuPage } from '../wdcashu/wdcashu';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';
/**
 * Generated class for the VisawithdrawalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visawithdrawal',
  templateUrl: 'visawithdrawal.html',
})
export class VisawithdrawalPage {
datas:any;
main:any;
loading:Loading;
get_rser:any;

formgroup: FormGroup;

account:AbstractControl;
Name:AbstractControl;
AccNumber:AbstractControl;
AmountWith:AbstractControl;
IBAN:AbstractControl;
swift:AbstractControl;
ifsc:AbstractControl;
BankName:AbstractControl;
BankAdd:AbstractControl;
ExtraInfo:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public feedbackserv:FeedbackService ,
  public serv:AuthServiceProvider,public alertCtrl:AlertController, public loadingCtrl :LoadingController,
  public formbuilder:FormBuilder, public network:Network) {
    this.datas=this.feedbackserv.AccountData.account;
    this.main=this.navParams.get('datas');

    this.formgroup = formbuilder.group({
      account:[Validators.required],
      AmountWith:[Validators.required],
      Name:["",Validators.required],
      AccNumber:["",Validators.required],
      IBAN: ["",Validators.required], 
      swift: ["",Validators.required], 
      ifsc: ["",Validators.required], 
      BankName: ["",Validators.required], 
      BankAdd: [null,Validators.required], 
      ExtraInfo: [null,Validators.required], 



    });
    
  

    this.account= this.formgroup.contains['account'];
    this.Name= this.formgroup.contains['Name'];
    this.AccNumber= this.formgroup.contains['AccNumber'];
    this.AmountWith= this.formgroup.contains['AmountWith'];
    this.IBAN= this.formgroup.contains['IBAN'];
    this.swift= this.formgroup.contains['swift'];
    this.ifsc= this.formgroup.contains['ifsc'];
    this.BankName= this.formgroup.contains['BankName'];
    this.BankAdd= this.formgroup.contains['BankAdd'];
    this.ExtraInfo= this.formgroup.contains['ExtraInfo'];





  }

  VisaWithdrawalFun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo){

    if (this.serv.is_user_authenticated() == false) {
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'You are logged out. Please Login again.',
        buttons: ['Ok']
      });
      alert.present();
      // this.splash.showError("You are logged out. Please Login again.");
      this.navCtrl.setRoot(LoginPage);
      return;
    }

    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }
  else{
    this.loading=this.loadingCtrl.create({
      content:'Please wait',
      // duration:1000
    })
    this.loading.present();



    this.serv.VisaWithdrawalfun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo).subscribe(data=>{
     this.get_rser=data;
     
      if (this.get_rser.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.get_rser.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle: this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }

      if (this.get_rser.status == "success") {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle:  this.get_rser.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
    },error => {
      this.loading.dismiss();

      // this.splash.showError(error);
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'error',
        buttons: ['Ok']
      });
      alert.present();
    
  })
}
  }
 
  homefun17(){
    this.navCtrl.setRoot(WithdrawalPage,{icon:this.main});
  }

  onKeyPress(event) {
    if ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122) || event.keyCode == 32 || event.keyCode == 46) {
        return true;
    }
    else {
        return false;
    }
}
}
