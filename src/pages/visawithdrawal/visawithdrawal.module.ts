import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisawithdrawalPage } from './visawithdrawal';

@NgModule({
  declarations: [
    VisawithdrawalPage,
  ],
  imports: [
    IonicPageModule.forChild(VisawithdrawalPage),
  ],
})
export class VisawithdrawalPageModule {}
