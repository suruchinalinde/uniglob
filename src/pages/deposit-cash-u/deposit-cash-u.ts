import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DepositPage } from '../deposit/deposit';
import {PersonalInfoService} from '../../providers/personal-info-service';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-deposit-cash-u',
  templateUrl: 'deposit-cash-u.html',
})
export class DepositCashUPage {
  firstName: any;
  lastName: any;
  phoneNumber: any;
  zipCode: any;
  country: any;
  city: any;
  email:any;
main:any;

formgroup:FormGroup;

firstNamee:AbstractControl;
lastNamee:AbstractControl;
phoneNumbere:AbstractControl;
zipCodee:AbstractControl;
countrye:AbstractControl;
citye:AbstractControl;
emaile:AbstractControl;
Amounte:AbstractControl;
sCurrencye:AbstractControl;


  constructor(public navCtrl: NavController, public navParams: NavParams,
  public personalInfoSerc:PersonalInfoService, public serv:AuthServiceProvider,public formbuilder:FormBuilder) {
    console.log(this.personalInfoSerc.getinfo);
    this.firstName=this.personalInfoSerc.getinfo.firstName;
    this.lastName=this.personalInfoSerc.getinfo.lastName;
    this.phoneNumber=this.personalInfoSerc.getinfo.phoneNumber;
    this.zipCode=this.personalInfoSerc.getinfo.zipCode;
    this.country=this.personalInfoSerc.getinfo.country;
    this.city=this.personalInfoSerc.getinfo.city;
    this.email=this.serv.currentUser.userEmail;
    
    this.main=this.navParams.get('datas');


    this.formgroup = formbuilder.group({
      //ewalletl:[Validators.required],
      firstNamee:['',Validators.required],
      lastNamee:['',Validators.required],
      phoneNumbere:['',Validators.required],
      zipCodee:['',Validators.required],
      countrye:['',Validators.required],
      citye:['',Validators.required],
      emaile:[Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      Amounte:[Validators.required],
      sCurrencye:['',Validators.required],

    });
    
    // newComment = new FormControl('', [validateEmptyField]);

this.firstNamee= this.formgroup.contains['firstNamee'];
this.lastNamee= this.formgroup.contains['lastNamee'];
this.phoneNumbere= this.formgroup.contains['phoneNumbere'];
this.zipCodee= this.formgroup.contains['zipCodee'];
this.countrye= this.formgroup.contains['countrye'];
this.citye= this.formgroup.contains['citye'];
this.emaile= this.formgroup.contains['emaile'];
this.Amounte=this.formgroup.contains['Amounte'];
this.sCurrencye=this.formgroup.contains['sCurrencye'];

    //this.comment= this.formgroup.contains['comment'];

  }
  homefun12(){
    this.navCtrl.setRoot(DepositPage,{icon:this.main});
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  

}
