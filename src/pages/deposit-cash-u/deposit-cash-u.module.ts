import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepositCashUPage } from './deposit-cash-u';

@NgModule({
  declarations: [
    DepositCashUPage,
  ],
  imports: [
    IonicPageModule.forChild(DepositCashUPage),
  ],
})
export class DepositCashUPageModule {}
