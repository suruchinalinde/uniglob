import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
// import {SplashComponent} from '../../providers/splash-component';
import { AlertController, LoadingController, Loading } from 'ionic-angular';
import { FormControl,FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';



@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  passWords:any;
  response:any;
  loading:Loading;

  formgroup:FormGroup

  oldPassword:AbstractControl;
  newPassword:AbstractControl;
  confirmPassword:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public serv:AuthServiceProvider,
private loadingCtrl: LoadingController,public alertCtrl:AlertController , public formbuilder:FormBuilder,
public network:Network) {
    this.passWords={
      oldPassword:'',
      newPassword:'',
      confirmPassword:''
    }

    this.formgroup = formbuilder.group({
      oldPassword:[Validators.required],
      newPassword:[Validators.required],
      confirmPassword:[Validators.required],


    });
    
    // newComment = new FormControl('', [validateEmptyField]);

    this.oldPassword= this.formgroup.contains['oldPassword'];
    this.newPassword= this.formgroup.contains['newPassword'];
    this.confirmPassword= this.formgroup.contains['confirmPassword'];
  

  }

 homefun2(){
   this.navCtrl.setRoot(HomePage);
 }


 passwordfun(){
  if(this.network.type === 'none')
  {
let alert = this.alertCtrl.create({
  title: ' Sorry ! No Internet Connection ',
  subTitle:'Please Connect To Internet And Try Again ',
  buttons: ['ok']
});
alert.present();
}
else{
  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    duration: 1000
  });
  this.loading.present();

   this.serv.changePasswordfun(this.passWords).subscribe(res=>{
     this.loading.dismiss();
    //  console.log(res);
    //  console.log(res.msg);
this.response=res.msg;
// () => {
  // this.loading.dismiss();
  if (this.response.status == 'success') {
    let alert = this.alertCtrl.create({
      // title: 'Success',
      subTitle: this.response,
      buttons: [{
        text:"Ok",
        handler:()=>{
          this.navCtrl.setRoot(ChangePasswordPage);
        }
      }]
    });
    alert.present();
    // this.splash.showSuccess(this.response.msg);
  }
  else {
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: this.response,
      buttons: [{
        text:"Ok",
        handler:()=>{
          this.navCtrl.setRoot(ChangePasswordPage);
        }
      }]
    });
    alert.present();
    // this.splash.showError(this.response.msg);
  }
}
     
// }
   );
  }
 }

}
