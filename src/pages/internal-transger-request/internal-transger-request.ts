import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {  AlertController,LoadingController,Loading} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl ,FormBuilder} from '@angular/forms';
import {Network} from '@ionic-native/network';
/**
 * Generated class for the InternalTransgerRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-internal-transger-request',
  templateUrl: 'internal-transger-request.html',
})
export class InternalTransgerRequestPage {
  save_response:any;
  loading:Loading;
formgroup: FormGroup;
fromaccount:AbstractControl;
toaccount:AbstractControl;
amount:AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public serv:AuthServiceProvider,
    public loadingCtrl:LoadingController, public alertCtrl:AlertController , public formbuilder:FormBuilder,
  public network:Network) {
      this.formgroup = formbuilder.group({
        fromaccount:[Validators.required, Validators.minLength(15)],
        toaccount:[Validators.required, Validators.minLength(15)],
        amount:[Validators.required, Validators.minLength(15)]
      });

      this.fromaccount= this.formgroup.contains['fromaccount'];
      this.toaccount= this.formgroup.contains['toaccount'];
      this.amount= this.formgroup.contains['amount'];

  }
  homefun5(){
    this.navCtrl.setRoot(HomePage);
  }


  internaltrasnferFun(fromaccount,toaccount,amount){
    if(this.network.type === 'none')
    {
  let alert = this.alertCtrl.create({
    title: 'Sorry ! No Internet Connection ',
    subTitle:'Please Connect To Internet And Try Again ',
    buttons: ['ok']
  });
  alert.present();
  }

  else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      // duration:1500
  
    });
    this.loading.present();

    this.serv.InternaltransferaMount(fromaccount,toaccount,amount).subscribe(data=>{
      // this.loading.dismiss();
      this.save_response = data;
      console.log(data);
      // this.loading.dismiss();



      if (this.save_response.status == 'T_EMPTY') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Error',
          subTitle:this.save_response.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        // this.navCtrl.setRoot(LoginPage);
        return;
      }
      else if (this.save_response.status == 'T_INVAL') {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({

          // title: 'Error',
          subTitle: this.save_response.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(LoginPage);
            }
          }]
        });
        alert.present();
        // this.splash.showError("You are logged out. Please Login again.");
        return;
      }
    
      if (this.save_response.status == "success") {
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.save_response.message,
          buttons: [{
            text:'Ok',
            handler:()=>{
              this.navCtrl.setRoot(HomePage);
            }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
      else{
        this.loading.dismiss();

        let alert = this.alertCtrl.create({
          // title: 'Success',
          subTitle: this.save_response.message,
          buttons: [{
            text:'Ok',
            // handler:()=>{
            //   this.navCtrl.setRoot(InternalTransgerRequestPage);
            // }
          }]
        });
        alert.present();
        // this.splash.showSuccess("Information saved successfully.");
        // this.navCtrl.setRoot(HomePage);
      }
    

    }, error =>{
      this.loading.dismiss();
      let alert = this.alertCtrl.create({
        // title: 'Error',
        subTitle: 'error',
        buttons: ['OK']
      });
      alert.present();
    //  this.splash.showError(error),
    },)
  }
  }
}
