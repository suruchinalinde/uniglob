import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InternalTransgerRequestPage } from './internal-transger-request';

@NgModule({
  declarations: [
    InternalTransgerRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(InternalTransgerRequestPage),
  ],
})
export class InternalTransgerRequestPageModule {}
