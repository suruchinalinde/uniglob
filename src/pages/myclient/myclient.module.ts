import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyclientPage } from './myclient';

@NgModule({
  declarations: [
    MyclientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyclientPage),
  ],
})
export class MyclientPageModule {}
