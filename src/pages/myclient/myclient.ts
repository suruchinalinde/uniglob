import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,Loading,LoadingController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { FundingPage } from '../funding/funding';

/**
 * Generated class for the MyclientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myclient',
  templateUrl: 'myclient.html',
})
export class MyclientPage {
  topics:string[];
  List:any[];
loading:Loading;
myInput:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public serv:AuthServiceProvider,
  public alertCtrl:AlertController,public loadingCtrl:LoadingController) {
    this.MylientTransacationfun();
    this.List=[];
    this.topics=[];
  }



public MylientTransacationfun()
{
  if (this.serv.is_user_authenticated() == false) {
    let alert = this.alertCtrl.create({
      // title: 'Error',
      subTitle: 'You are logged out. Please Login again.',
      buttons: ['OK']
    });
    alert.present();
    // this.splash.showError("You are logged out. Please Login again.");
    this.navCtrl.setRoot(LoginPage);
    return;
  }

  // this.splash.showLoading();
  //  this.navCtrl.setRoot(PersonalInfoPage);

  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    duration:2000
  });
  this.loading.present();

this.serv.MyClientTransactionFun().subscribe(data=>{
  console.log(data.clients);
this.List=data.clients;
this.topics=this.List;
  this.loading.dismiss();
})
}
homefun66(){
  this.navCtrl.setRoot(HomePage);
}

onSearchInput($event){
  this.topics=Array();
  console.log(this.topics)
  for (var i = 0; i < this.List.length; i++) {
    
          if (this.List[i].account_no_1.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].FirstName.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].LastName.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].UserEmail.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].PhoneNumber.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.List[i].country.toLowerCase().search(this.myInput.toLowerCase()) != -1 ||  this.List[i].city.toLowerCase().search(this.myInput.toLowerCase()) != -1 ) {
            this.topics.push(this.List[i]);
          }
        }
    
        if (this.topics.length == 0) {
        }
}
homefun11(){



    this.navCtrl.setRoot(FundingPage);
  


  
}
}
