import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UrlProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UrlProvider {
 
Url:any;
  loginUrl:any;
  getPersonal_InfoUrl:any;
  PostFeedbackUrl:any;
postRequsetUrl:any;
depositskrillURL:any;
changePasswordUrl:any;
CallBackUrl:any;
getAccountUrl:any;
getdataUrl:any;
InternaltransferaMountUrl:any;
EwalletURL:any;
withdwalUrl:any;
VisaWithdrawalurl:any;
BankWithdrawalurl:any;
GetTransactionInfoUrl:any;
NetellerUrl:any;
PartnerPersonalInfoUrl:any;
ForgetPassUrl:any;
MyClientTransactionUrl:any;
TrackaffiliateUrl:any;
AffiliateMyClientUrl:any;


  constructor(public http: HttpClient)
   {
// this.Url="http://192.168.1.202/projects/uniglobalmarket/user/api/"
this.Url="https://www.uniglobemarkets.com/unibeta/user/api/";

this.loginUrl=this.Url+"login.php";
this.PostFeedbackUrl=this.Url+"feedback.php";
this.postRequsetUrl=this.Url+"personal_info.php";
this.changePasswordUrl=this.Url+"change_password.php";
this.CallBackUrl=this.Url+"callback.php";
this.getAccountUrl=this.Url+"getAccounts.php";  
this.getdataUrl=this.Url+"requestNewAccount.php";
this.InternaltransferaMountUrl=this.Url+"internal-transfer-request.php";
this.EwalletURL=this.Url+"getEwallets.php";
this.withdwalUrl=this.Url+"ewalletwithdrawals.php";
this.VisaWithdrawalurl=this.Url+"cardwithdrawals.php";
this.BankWithdrawalurl=this.Url+"bankwithdrawals.php";
this.GetTransactionInfoUrl=this.Url+"getTransactionhistory.php";
this.NetellerUrl=this.Url+"paymentgateway/netellerapi.php";
this.PartnerPersonalInfoUrl=this.Url+"personal_infoagent.php";
this.ForgetPassUrl=this.Url+"forgot-password.php";
this.MyClientTransactionUrl=this.Url+"getagentclients.php";
this.TrackaffiliateUrl=this.Url+"trackaffilieat.php";
this.AffiliateMyClientUrl=this.Url+"getaffilieatclients.php";
}

}
