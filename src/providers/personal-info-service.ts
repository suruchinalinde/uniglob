import { Observable } from 'rxjs/Rx';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { UrlProvider } from '../providers/url/url';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

 export class getUserInfo{
  firstName: any;
  middleName: any;
  lastName: any;
  alternateNumber:any;
  phoneNumber: any;
  citizenShip: any;
  streetNumber: any;
  stateProvince: any;
  address: any;
  zipCode: any;
  dateOfBirth: any;
  country: any;
  gender: any;
  city: any;
  details: any;
  referenceMedium: any;
  countrycode:any;
  UserEmail:any;
  primarymethod:any;
  other_method:any;
  business_info:any;
  ib_exp:any;
  estimateno:any;

   constructor(userinfo){
    this.firstName = userinfo.FirstName;
    this.middleName = userinfo.MiddleName;
    this.lastName = userinfo.LastName;
     this.alternateNumber=userinfo.AltPhoneNumber;
     this.phoneNumber=userinfo.PhoneNumber;
     this.citizenShip=userinfo.citizenShip;
     this.streetNumber=userinfo.streetNumber;
     this.stateProvince=userinfo.state;
     this.address=userinfo.Address;
     this.zipCode=userinfo.zip;
     this.dateOfBirth=userinfo.dob;
     this.country=userinfo.country;
     this.gender=userinfo.gender;
     this.city=userinfo.city;
     this.details=userinfo.details;
     this.referenceMedium=userinfo.affiliate;
      this.countrycode=userinfo.countrycode;
this.UserEmail=userinfo.email;
     this.primarymethod=userinfo.primarymethod;
         this.other_method=userinfo.other_method;
     this.business_info=userinfo.business_info;
     this.ib_exp=userinfo.ib_exp;
     this.estimateno=userinfo.estimateno;
   }

 }



export class UserInfo {

  

  personalInfo: any;

  addressDetails: any;

  constructor() {

    this.personalInfo = {
      firstName: '',
      middleName: '',
      lastName: '',
      alternateNumber: '',
      phoneNumber: '',
      altemail:''

    };

    this.addressDetails = {
      citizenShip: '',
      streetNumber: '',
      stateProvince: '',
      address: '',
      zipCode: '',
      dateOfBirth: '',
      country: '',
      gender: '',
      city: '',
      details: '',
      referenceMedium: '',
      countrycode:'',
      UserEmail:'',
      primarymethod:'',
      other_method:'',
      business_info:'',
      ib_exp:'',
      estimateno:''
      
      
    };
  }

}


@Injectable()
export class PersonalInfoService {
userInfo: UserInfo;
  postRequsetUrl: string;
  authToken: string;
  requestBody: any;
  getinfo:any;
  constructor(public http: Http, public serv: AuthServiceProvider, public url: UrlProvider) {

    this.postRequsetUrl = this.url.postRequsetUrl;


    // save user info so that requests to server are minimal. 
    this.userInfo = new UserInfo();
  }

  public get_personal_info(user_type) {
console.log(user_type);

      // set action for the api. 
      var api_action = "get";
  
      // Return Observable.
      // var personObser: any;
  
      // Get current users' authentication token.
      this.authToken = this.serv.getAuthToken();
      this.requestBody = { 
        token: this.authToken,
         action: api_action,
         user_type:user_type
         }
         console.log(this.requestBody);
     return this.http.post(this.postRequsetUrl, JSON.stringify(this.requestBody))
        .map(response => response.json());
  
   
    
  }
 


  public save_personal_info(personalInfo, addressDetails, user_type) {

    var personalObser: any;
    var action = "save";

    this.requestBody = { 
      token: this.authToken,
       action: action,
        info:personalInfo,
         addrInfo: addressDetails ,
         user_type:user_type
        }
        console.log(this.requestBody);
    personalObser = this.http.post(this.postRequsetUrl, JSON.stringify(this.requestBody))
      .map(response => response.json())

    return personalObser;
  }


    populateUserInfo(response) {
      
    this.userInfo.personalInfo.firstName = response.user.FirstName;
    this.userInfo.personalInfo.lastName = response.user.LastName;
    this.userInfo.personalInfo.middleName = response.user.MiddleName;
    this.userInfo.personalInfo.alternateNumber = response.user.AltPhoneNumber;
    this.userInfo.personalInfo.phoneNumber = response.user.PhoneNumber;
    this.userInfo.personalInfo.altemail = response.user.altemail;
    this.userInfo.addressDetails.citizenShip = response.user.citizenship;
    this.userInfo.addressDetails.dateOfBirth =response.user.dob;
    this.userInfo.addressDetails.stateProvince =response.user.state;
    this.userInfo.addressDetails.address =response.user.Address;
    this.userInfo.addressDetails.streetNumber = response.user.street_num;
    this.userInfo.addressDetails.referenceMedium = response.user.affiliate;
    this.userInfo.addressDetails.country = response.user.country;
    this.userInfo.addressDetails.city = response.user.city;
    this.userInfo.addressDetails.zipCode = response.user.zip;
    this.userInfo.addressDetails.gender = response.user.gender;
    this.userInfo.addressDetails.details = response.user.details;
    this.userInfo.addressDetails.countrycode = response.user.countrycode;
    this.userInfo.addressDetails.business_info=response.user.business_info;
    this.userInfo.addressDetails.ib_exp=response.user.ib_exp;
    this.userInfo.addressDetails.estimateno=response.user.estimateno;
this.userInfo.addressDetails.account1=response.user.account1;
this.userInfo.addressDetails.UserEmail=response.user.email;
this.userInfo.addressDetails.primarymethod=response.user.primarymethod;
this.userInfo.addressDetails.other_method=response.user.other_method;


    this.serv.user_info_saved = 1;
  }

  public setUser(response) {
    console.log(response);
    this.getinfo = new getUserInfo(response.user);
  

  }
}
