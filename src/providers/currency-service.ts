import { AuthServiceProvider} from '../providers/auth-service/auth-service'

import { UrlProvider } from '../providers/url/url';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CurrencyService  {
currency:any;
yearm:any;

  constructor(public http: Http, public url: UrlProvider, public serv: AuthServiceProvider) {
  this.currency=[
    {
      "name":"USD"
    },
    {
      "name":"AUG"
    },
    {
      "name":"GBP"
    },
    {
      "name":"BGN"
    },
    {
      "name":"CAD"
    },
    {
      "name":"DKK"
    },
    {
      "name":"EUR"
    },
    {
      "name":"HUF"
    },
    {
      "name":"INR"    
    },
    {
      "name":"JPY"    
    },
    {
      "name":"MYR"    
    },
    {
      "name":"MAD"    
    },
    {
      "name":"MXN"    
    },
    {
      "name":"NGN"    
    },
    {
      "name":"NOK"    
    },
    {
      "name":"PLN"    
    },
    {
      "name":"RON"    
    },
    {
      "name":"RUB"    
    },
    {
      "name":"SGD"    
    },
    {
      "name":"SEK"    
    },
    {
      "name":"CHF"    
    },
    {
      "name":"TWD"    
    },
 
  ],

    this.yearm=[
     
      {
        "year":"2018"
      },
      {
        "year":"2019"
      },
      {
        "year":"2020"
      },
      {
        "year":"2021"
      },
      {
        "year":"2022"
      },
      {
        "year":"2023"
      },
      {
        "year":"2024"
      },
      {
        "year":"2025"
      },
      {
        "year":"2026"
      },
      {
        "year":"2027"
      },
      {
        "year":"2028"
      },
      {
        "year":"2029"
      },
      {
        "year":"2030"
      },
      {
        "year":"2031"
      },
      {
        "year":"2032"
      },
      {
        "year":"2033"
      },
      {
        "year":"2034"
      },
      {
        "year":"2035"
      },
      {
        "year":"2036"
      },
      {
        "year":"2037"
      },
      {
        "year":"2038"
      },
      {
        "year":"2039"
      },
      {
        "year":"2040"
      },
      {
        "year":"2041"
      },
      {
        "year":"2042"
      },
      {
        "year":"2043"
      },
      {
        "year":"2044"
      },
      {
        "year":"2045"
      },
      {
        "year":"2046"
      },
      {
        "year":"2047"
      },
      {
        "year":"2048"
      },
      {
        "year":"2048"
      },
      {
        "year":"2050"
      },
     


    ];
  }



}
