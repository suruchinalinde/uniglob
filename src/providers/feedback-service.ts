import { AuthServiceProvider} from '../providers/auth-service/auth-service'

import { UrlProvider } from '../providers/url/url';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

export class  accountdata{
  account:any;
  constructor(user){
    this.account=user.accounts;
  }
}


@Injectable()
export class FeedbackService {

  PostFeedbackUrl: string;

  accountsSaved = 0;
  AccountData:any;
  accounts:any;

  constructor(public http: Http, public url: UrlProvider, public serv: AuthServiceProvider) {
    this.PostFeedbackUrl = this.url.PostFeedbackUrl;
  }

  get_accounts() {

console.log(this.serv.authToken);
    var requestBody= {
      token : this.serv.authToken,
      action : 'get'
    }

   return this.http.post(this.PostFeedbackUrl, JSON.stringify(requestBody))
      .map(response => response.json());

   
  }

  populate_accounts(response) {
    this.accounts = response.accounts;
    this.accountsSaved = 1;
  }


  send_feedback(feedback) {

   
    var requestBody= {
      token : this.serv.authToken,
      action : 'send',
      feedback: feedback
    }
console.log(requestBody);
return this.http.post(this.PostFeedbackUrl, JSON.stringify(requestBody))
      .map(response => response.json());

     
  }


public setAcountId(data){
  this.AccountData= new accountdata(data);

}
}
