import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {UrlProvider} from '../url/url';
import {Http, Jsonp} from '@angular/http';
import { IonicPage, NavController, NavParams , AlertController,LoadingController} from 'ionic-angular';
import {Network} from '@ionic-native/network';

import 'rxjs/Rx';
import { Token, ReturnStatement } from '@angular/compiler';

export class User {
  firstName: string;
  middleName: string;
  lastName: string;
  role: string;
  userEmail: string;
  document_verify:any;
  affiliatecode:any;
  UserID:any;

Token:any;
id:any;
  constructor(user) {
    this.firstName = user.FirstName;
    this.middleName = user.MiddleName;
    this.lastName = user.LastName;
    this.userEmail = user.UserEmail;
    this.role = user.role;
    this.id=user.UserID;
    this.document_verify=user.document_verify;
    this.affiliatecode=user.affiliatecode;
 this.Token=user.token_info.token;
 this.UserID=user.UserID;
  }
}

@Injectable()
export class AuthServiceProvider {
  UserID: any;
  currentUser: User;
  loginUrl:any;
  getPersonal_InfoUrl:any;
  PostFeedbackUrl:any;
  authToken:any;
  postRequsetUrl:any;
  getperonalinfodata:any;
  changePasswordUrl:any;
  user_info_saved:any;
  CallBackUrl:any;
  getAccountUrl:any;
  getdataUrl:any;
  InternaltransferaMountUrl:any;
  EwalletURL:any;
  withdwalUrl:any;
  VisaWithdrawalurl:any;
  BankWithdrawalurl:any;
  GetTransactionInfoUrl:any;
  NetellerUrl:any;
  PartnerPersonalInfoUrl:any;
  ForgetPassUrl:any;
  MyClientTransactionUrl:any;
  TrackaffiliateUrl:any;
  AffiliateMyClientUrl:any;


  constructor(public httpclient: HttpClient, public url:UrlProvider, public http:Http, public network:Network,
  public alertCtrl:AlertController) {
    this.user_info_saved=0;

    
this.loginUrl=this.url.loginUrl;
this.getPersonal_InfoUrl=this.url.getPersonal_InfoUrl;
this.PostFeedbackUrl=this.url.PostFeedbackUrl;
this.postRequsetUrl=this.url.postRequsetUrl;
this.changePasswordUrl=this.url.changePasswordUrl;
this.CallBackUrl=this.url.CallBackUrl;
this.getAccountUrl=this.url.getAccountUrl;
this.getdataUrl=this.url.getdataUrl;
this.InternaltransferaMountUrl=this.url.InternaltransferaMountUrl;
this.EwalletURL=this.url.EwalletURL;
this.withdwalUrl=this.url.withdwalUrl;
this.VisaWithdrawalurl=this.url.VisaWithdrawalurl;
this.BankWithdrawalurl=this.url.BankWithdrawalurl; 
this.GetTransactionInfoUrl=this.url.GetTransactionInfoUrl;
this.NetellerUrl=this.url.NetellerUrl;
this.PartnerPersonalInfoUrl=this.url.PartnerPersonalInfoUrl;
this.ForgetPassUrl=this.url.ForgetPassUrl;
this.MyClientTransactionUrl=this.url.MyClientTransactionUrl;
this.TrackaffiliateUrl=this.url.TrackaffiliateUrl;
this.AffiliateMyClientUrl=this.url.AffiliateMyClientUrl;
}

  public loginfun(email,password,usertype){

//     if(network.type === 'none')
//     {
//   let alert = this.alertCtrl.create({
//     title: 'Sorry ! No Internet Connection ',
//     subTitle:'Please Connect To Internet And Try Again ',
//     buttons: ['ok']
//   });
//   alert.present();
// }

    console.log(email,password,usertype);
var mm;

    var body ={
      
email:email,
password:password,
userType:usertype,

    }
    
   mm= this.http.post(this.loginUrl,JSON.stringify(body)).map(res=>  res.json() );
   return mm;

  }

  public setAuthToken(data) {

    this.authToken = data.token_info.token;
    this.UserID=data.UserID;
    console.log(this.authToken);
   
  }

  public getAuthToken() {
    return this.authToken;


  }



  
  public setUser(data) {
    this.currentUser = new User(data);
  
  }

// public postfeedback(Sendfeedback){
// var mm;
//   var feedback={
//     token:this.authToken, 
//     action:'send',
//     feedback:Sendfeedback
//   }
 

//  mm=  this.http.post(this.PostFeedbackUrl,JSON.stringify(feedback)).map(res=>res.json());
//   return mm;
  
// }

public changePasswordfun(passWords){
  console.log(passWords,this.authToken);
  var changePasswordbody={
    // stauts:'T_INVAL',
token:this.authToken,
passwords:passWords
  }
  return this.http.post(this.changePasswordUrl,JSON.stringify(changePasswordbody)).map(res=>res.json());
}

public get_account(){
  var body ={
token:this.authToken,
action:'get'
  }
  return this.http.post(this.PostFeedbackUrl,JSON.stringify(body)).map(res=>res.json());
}

public logout() {
  this.authToken = '';
  this.user_info_saved = 0;
}

public is_user_authenticated() {
  if (this.authToken != null) {
    return true;
  }
  else {
    return false;
  }
}


public callback(phonenumber,timeslot){

  var body={
    token:this.authToken,
    phonenumber:phonenumber,
    timeslot:timeslot,
    action:'send'
   
  }
  console.log(body);
  return this.http.post(this.CallBackUrl,JSON.stringify(body)).map(res=>res.json());
}



public getaccountInfo(){ 
  var body={
token:this.authToken
  }
  return this.http.post(this.getAccountUrl,JSON.stringify(body)).map(res=>res.json());
}


 public getnewdata(){
   var body={
    token:this.authToken,
     action:'get'

   }
 return this.http.post(this.getdataUrl,JSON.stringify(body)).map(res=>res.json());
 }

 public sendnewACrequestt(accountrype, requestoffer, tradingplatform){
   console.log(accountrype, requestoffer, tradingplatform);
   var body={
    token:this.authToken,
     action:'send',
     accountrype:accountrype, 
     requestoffer:requestoffer,
     tradingplatform:tradingplatform

   }
  return this.http.post(this.getdataUrl,JSON.stringify(body)).map(res=>res.json());
 }


 public InternaltransferaMount(fromaccount,toaccount,amount){
   console.log(fromaccount,toaccount,amount);
   var body={
    token:this.authToken,
    action:'send',
    toaccount:toaccount,
    fromaccount:fromaccount,
    amount:amount,
    // action:'send'

   }
return this.http.post(this.InternaltransferaMountUrl,JSON.stringify(body)).map(res=>res.json());
  }

  public getEwallet(){
    var body={
      token:this.authToken,
      

    }
    return this.http.post(this.EwalletURL,JSON.stringify(body)).map(res=>res.json());
  }


  public  PostEwalletFUN(ewallet,account,email,amount,comment){
    console.log(ewallet,account,email,amount,comment);
  var body={
      token:this.authToken,
      ewallet:ewallet,
      account:account,
      email:email,
      amount:amount,
      comment:comment,
      action:'send',

  }
  console.log(body);
  return this.http.post(this.withdwalUrl,JSON.stringify(body)).map(res=>res.json());
}

public VisaWithdrawalfun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo){
  console.log(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo);
  var body={
    action:"send",
    token:this.authToken,
    account:account,
    Name:Name,
    AccNumber:AccNumber,
    AmountWith:AmountWith,
    IBAN:IBAN,
    swift:swift,
    ifsc:ifsc,
    BankName:BankName,
    BankAdd:BankAdd,
    ExtraInfo:ExtraInfo
  }
  return this.http.post(this.VisaWithdrawalurl,JSON.stringify(body)).map(res=>res.json());
}

public BankWithdrawalfun(account,Name,AccNumber,AmountWith,IBAN,swift,ifsc,BankName,BankAdd,ExtraInfo){
  var body={
    action:"send",
    token:this.authToken,
    account:account,
    Name:Name,
    AccNumber:AccNumber,
    AmountWith:AmountWith,
    IBAN:IBAN,
    swift:swift,
    ifsc:ifsc,
    BankName:BankName,
    BankAdd:BankAdd,
    ExtraInfo:ExtraInfo
  }
  return this.http.post(this.BankWithdrawalurl,JSON.stringify(body)).map(res=>res.json());
}

public GetTransactionInfoFun(){
  var body={
token:this.authToken
  }
 
  return this.http.post(this.GetTransactionInfoUrl,JSON.stringify(body)).map(res=>res.json());
}

Netellerfunction(accountid,amount,currency,code){
  console.log(accountid,amount,currency,code);
var body ={
action :'send',
token :this.authToken,
accountid:accountid,
amount:amount,
currency:currency,
code:code

}
console.log(body);
return this.http.post(this.NetellerUrl,JSON.stringify(body)).map(res=>res.json());
}

public ForgetPassFun(UserEmail,user_type){
  var body={
    UserEmail:UserEmail,
    user_type:user_type
  }
  return this.http.post(this.ForgetPassUrl,JSON.stringify(body)).map(res=>res.json());
}

MyClientTransactionFun(){
var body={
  token:this.authToken,
  UserID:this.UserID
}
return this.http.post(this.MyClientTransactionUrl,JSON.stringify(body)).map(res=>res.json());
}

public Trackaffiliatefun(){
  var body={
token:this.authToken,
UserID:this.UserID
  }
  return this.http.post(this.TrackaffiliateUrl,JSON.stringify(body)).map(res=>res.json());
}

AffiliateMyClientFun(){
var body={
token:this.authToken,
UserID:this.UserID
}
return this.http.post(this.AffiliateMyClientUrl,JSON.stringify(body)).map(res=>res.json());
}

public localstorge(){
  localStorage=this.authToken;
  localStorage=this.UserID;
}

public visausrl(){
  var body={
    token:this.authToken
  }
  return this.http.post('https://www.uniglobemarkets.com/unibeta/user/api/paymentgateway/wirecapital/index.php',JSON.stringify(body)).map(res=>res.json());
}

}


